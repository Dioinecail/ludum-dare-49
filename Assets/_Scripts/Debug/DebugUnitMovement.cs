﻿namespace Project.CustomDebug
{
    using Project.Units;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class DebugUnitMovement : MonoBehaviour
    {
        [SerializeField] private UnitManager unitManager;
        [SerializeField] private UnitStateSettings settings;
        [SerializeField] private bool displayAvoidanceRadius;

        private Vector2 lastMousePosition;
        private IUnit customTarget;



        private void Update()
        {
            if(Input.GetMouseButton(0))
            {
                SetUnitPosition();
            }
        }

        private void SetUnitPosition()
        {
            lastMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if(customTarget == null)
            {
                Dictionary<StatType, int> stats = new Dictionary<StatType, int>() { { StatType.HP, 999999999}, { StatType.ATK, 0 }, { StatType.ATKRANGE, 1}, { StatType.DEF, 10000 }, { StatType.MOVESPEED, 0 }, {StatType.ATKSPEED, 0 } };
                customTarget = unitManager.SpawnCustomUnit(lastMousePosition, Team.ENEMY, stats);
            }

            customTarget.Move(lastMousePosition);
        }

        private void OnDrawGizmos()
        {
            if (!displayAvoidanceRadius || customTarget == null)
                return;

            Gizmos.color = Color.yellow;

            Gizmos.DrawWireSphere(customTarget.Position, settings.avoidanceRadius);
        }
    }
}