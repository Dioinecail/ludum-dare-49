﻿namespace Project.CustomDebug
{

    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Project.Grid;
    using Sirenix.OdinInspector;
    using Project.Units;

    public class DebugGridManager : MonoBehaviour
    {
        [SerializeField] private bool debug;
        [SerializeField] private float gridSize;
        [SerializeField] private float cellSize;
        [SerializeField] private UnitManager unitManager;

        private int GridCellCount { get { return Mathf.FloorToInt(gridSize / cellSize); } }

        private GridManager gridManager { get { return unitManager.GridManager; } }



        //[Button]
        //private void CreateGrid()
        //{
        //    gridManager = new GridManager(GridCellCount, cellSize);
        //}
        private void OnDrawGizmos()
        {
            if (gridManager == null || !debug)
                return;

            for (int x = 0; x < gridManager.Size; x++)
            {
                for (int y = 0; y < gridManager.Size; y++)
                {
                    if (gridManager.grid[x, y].IsOccupied)
                        Gizmos.color = new Color(1, 1, 0, 1);
                    else
                        Gizmos.color = Color.gray;

                    Gizmos.DrawWireCube(gridManager.GetCellPosition(x, y), Vector2.one * cellSize * 0.9f);
                }
            }
        }

        private void OnValidate()
        {
            cellSize = Mathf.Abs(cellSize);
            gridSize = Mathf.Abs(gridSize);

            //gridManager = new GridManager(GridCellCount, cellSize);
        }
    }
}