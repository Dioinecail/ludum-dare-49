﻿namespace Project.CustomDebug
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Project.Units;
    using System;
    using Sirenix.OdinInspector;
    using TMPro;

    public class DebugUnitManager : MonoBehaviour
    {
        public UnitManager unitManager;
        public UnitSettings settings;
        public TMP_Text unitCountPlayer;
        public TMP_Text unitCountEnemy;
        public TMP_Text simulationsCompletedText;
        public TMP_Text winrateText;
        public TMP_Text averateSurvivorsText;

        public float boundsX, boundsY;
        public float unitRadius;

        public bool displayHP;
        public bool displayATK;
        public bool displayDEF;
        public bool displayATKRANGE;
        public bool displayMOVESPEED;

        private int timesPlayerWon;
        private int simulationsCompleted;
        private int averageSurvivors;
        private int playerCount;



        private void OnUnitCountChanged(Team team, int count)
        {
            if (team == Team.PLAYER)
            {
                unitCountPlayer.text = $"Players: {count}";
                playerCount = count;
            }
            else
                unitCountEnemy.text = $"Enemies: {count}";
        }

        private void OnTeamDied(Team team)
        {
            simulationsCompleted++;
            averageSurvivors += playerCount;

            if (team == Team.ENEMY)
            {
                timesPlayerWon++;
            }

            float winrate = ((float)timesPlayerWon / simulationsCompleted) * 100f;

            simulationsCompletedText.text = $"Simulations: {simulationsCompleted}";
            winrateText.text = $"WinRate: {winrate.ToString("#00")}%";

            if(timesPlayerWon > 0)
                averateSurvivorsText.text = $"Average survivors: {averageSurvivors / timesPlayerWon}";

            unitManager.Reset();
        }

        private void OnUnitDied(IUnit unit)
        {

        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireCube(Vector3.zero, new Vector3(2 * boundsX, 2 * boundsY));

            if (unitManager == null || !Application.isPlaying)
                return;

            IUnit[] allUnits = unitManager.GetUnits();

            foreach (IUnit unit in allUnits)
            {
                Gizmos.color = unit.UnitTeam == Team.PLAYER ? Color.red : Color.blue;
                Gizmos.DrawSphere(unit.Position, unitRadius);

                if(displayATKRANGE)
                {
                    Gizmos.color = Color.red;
                    Gizmos.DrawWireSphere(unit.Position, unit.GetStat(StatType.ATKRANGE) * settings.AttackRangeMult);
                }

                if(displayHP)
                {
                    Gizmos.color = new Color(0.5f, 0, 0, 1);
                    Gizmos.DrawLine(unit.Position + Vector2.up * unitRadius + Vector2.left * unitRadius, unit.Position + Vector2.up * unitRadius + Vector2.right * unitRadius);

                    Gizmos.color = new Color(1f, 0, 0, 1);
                    Gizmos.DrawLine(unit.Position + Vector2.up * unitRadius + Vector2.left * unitRadius, unit.Position + Vector2.up * unitRadius + Vector2.right * unitRadius * unit.CurrentHP);
                }
            }
        }

        private void OnEnable()
        {
            UnitManager.onUnitsCountChanged += OnUnitCountChanged;
            UnitManager.onAllUnitsDied += OnTeamDied;
            UnitManager.onUnitDied += OnUnitDied;
        }

        private void OnDisable()
        {
            UnitManager.onUnitsCountChanged -= OnUnitCountChanged;
            UnitManager.onAllUnitsDied -= OnTeamDied;
            UnitManager.onUnitDied -= OnUnitDied;
        }
    }
}