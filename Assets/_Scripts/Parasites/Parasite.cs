﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Project.Units;
using Project.Utility;

namespace Project.Parasites
{
    public class Parasite
    {
        #region Private Fields

        private List<DataParasite> _datas = new List<DataParasite>();

        private IUnit _unitParasite;

        #endregion

        #region Properties

        public List<ChangesStats> NewParasitesStats { get; private set; } = new List<ChangesStats>();

        public ActionEvent OnChangeParasite { get; private set; } = new ActionEvent();
        public ActionEvent<Vector3, int> OnBoomDeathParasite { get; private set; } = new ActionEvent<Vector3, int>();
        public ActionEvent<DataParasite, int, Vector3, float> OnSpawnDethParasite { get; private set; } = new ActionEvent<DataParasite, int, Vector3, float>();
        
        #endregion

        #region Public Methods

        public Parasite(DataParasite data, IUnit unit)
        {
            _unitParasite = unit;
            _datas.Add(data);
            NewParasitesStats.AddRange(data.ParasitesStats);
            OnChangeParasite.Invoke();
        }

        public void AddNewParasite(DataParasite dataParasite)
        {
            _datas.Add(dataParasite);
            NewParasitesStats.AddRange(dataParasite.ParasitesStats);
            OnChangeParasite.Invoke();
            //if(_datas[0].Type == dataParasite.Type)
            //{
            //}
            //else
            //{
            //    //_unitParasite.DealDamage(int.MaxValue);
            //}
        }

        public void DeathEvent()
        {
            //if (IsBoomParsites()) OnBoomDeathParasite.Invoke(_unitParasite.Position, _datas.Count);
            //else 
            //if (IsSpawnParsites())
            //{
            //    var parsite = RandomUtility.GetRandom(_datas);
            //    OnSpawnDethParasite.Invoke(parsite, _datas.Count, _unitParasite.Position, parsite.ParasitesStats.Count);
            //}

            ClearData();
        }

        #endregion

        #region Private Methods

        private void ClearData()
        {
            _unitParasite = null;
            _datas.Clear();
            NewParasitesStats.Clear();
            OnChangeParasite.RemoveAllListener();
            OnBoomDeathParasite.RemoveAllListener();
            OnSpawnDethParasite.RemoveAllListener();
        }

        private bool IsBoomParsites()
        {
            if ((NewParasitesStats.Count * _datas.Count) > Random.Range(0, 100)) return true;

            return false;
        }

        private bool IsSpawnParsites()
        {
            if (_datas.Count * 10 > Random.Range(0, 100)) return true;

            return false;
        }

        #endregion
    }

}