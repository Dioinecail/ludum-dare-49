﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Project.Units;

namespace Project.Parasites
{
    public enum TypeParsite
    {
        Type1,
        Type2,
        Type3,
        Type4,
        Type5
    }

    [System.Serializable]
    public class DataParasite
    {
        #region Inspector's Fields

        [SerializeField] private TypeParsite _type = TypeParsite.Type1;
        [SerializeField] private bool _onAllUnits = true;
        [SerializeField, HideIf("OnAllUnits")] private Team _teamType = Team.PLAYER;
        [SerializeField, InlineProperty] private List<ChangesStats> _parasitesStats = new List<ChangesStats>();

        #endregion

        #region Properties

        public TypeParsite Type => _type;
        public bool OnAllUnits => _onAllUnits;
        public Team TeamType => _teamType;
        public List<ChangesStats> ParasitesStats => _parasitesStats;

        #endregion
    }

    public enum StatOperation
    {
        Add = 0,
        Subtract,
        Multiply,
        Divide
    }

    [System.Serializable]
    public class ChangesStats
    {
        #region Inspector's Fields

        [HorizontalGroup("hor"), SerializeField, HideLabel] private StatType _statType = StatType.HP;
        [HorizontalGroup("hor"), SerializeField, HideLabel] private StatOperation _statOperation = StatOperation.Add;
        [HorizontalGroup("hor"), SerializeField, HideLabel] private int _value = 1;

        #endregion

        #region Properties

        public StatType StatType => _statType;
        public StatOperation StatOperation => _statOperation;
        public int Value => _value;

        #endregion

        #region Public Methods

        public override string ToString()
        {
            string operation = null;
            string stat = null;

            switch (StatType)
            {
                case StatType.HP:
                    stat = "HP";
                    break;
                case StatType.ATK:
                    stat = "ATK";
                    break;
                case StatType.DEF:
                    stat = "DEF";
                    break;
                case StatType.ATKRANGE:
                    stat = "ATKRANGE";
                    break;
                case StatType.MOVESPEED:
                    stat = "MOVESPEED";
                    break;
                case StatType.ATKSPEED:
                    stat = "ATKSPEED";
                    break;
                default:
                    break;
            }

            switch (StatOperation)
            {
                case StatOperation.Add:
                    operation = $"+{Value}{stat}";
                    break;
                case StatOperation.Subtract:
                    operation = $"-{Value}{stat}";
                    break;
                case StatOperation.Multiply:
                    operation = $"multiply {stat} by {Value}";
                    break;
                case StatOperation.Divide:
                    operation = $"divide {stat} by {Value}";
                    break;
                default:
                    break;
            }

            return operation;
        }

        #endregion
    }
}
