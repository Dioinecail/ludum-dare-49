﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Project.Units;
using Project.Inventory;
using System.Linq;

namespace Project.Parasites
{
    public class ParasitesManager : MonoBehaviour
    {
        public static event System.Action<Parasite> onParasiteUsed;

        #region Private Fields

        [SerializeField] private UnitManager _unitManager;

        #endregion

        #region Private Methods

        private void Awake()
        {
            DropItemToGameController.OnDropItemToPosition.AddListener(OnDropItemToGame);
            UnitManager.onUnitDied += OnUnitDead;
        }

        private void OnDestroy()
        {
            DropItemToGameController.OnDropItemToPosition.RemoveListener(OnDropItemToGame);
            UnitManager.onUnitDied -= OnUnitDead;
        }

        private void OnDropItemToGame(DataItem item, Vector2 position)
        {
            if (item == null) return;

            position = Vector3.ProjectOnPlane(Camera.main.ScreenToWorldPoint(position), Vector3.forward);

            TryGetParasites(item.Parasite, item.AmountParasites, position, item.Radius);
            onParasiteUsed?.Invoke(null);
        }

        private void TryGetParasites(DataParasite dataParasite, uint amountParasites, Vector3 position, float radius)
        {
            if (dataParasite == null) return;

            IUnit[] opposingUnits = null;

            if (dataParasite.OnAllUnits) opposingUnits = _unitManager.GetUnits();
            else
            {
                switch (dataParasite.TeamType)
                {
                    case Team.PLAYER:
                        opposingUnits = _unitManager.GetUnits(Team.PLAYER);
                        break;
                    case Team.ENEMY:
                        opposingUnits = _unitManager.GetUnits(Team.ENEMY);
                        break;
                }
            }

            IUnit[] targets = FindUnitsInRadios(position, radius, opposingUnits);

            var infectedCount = 0;
            foreach (var target in targets)
            {
                TrySetParasiteToUnit(target, dataParasite);
                infectedCount++;
                if (infectedCount >= amountParasites) break;
            }
        }

        private IUnit[] FindUnitsInRadios(Vector3 position, float radius, IUnit[] units)
        {
            List<IUnit> targets = new List<IUnit>();

            foreach (IUnit tgt in units)
            {
                float currentDist = Vector3.Distance(position, tgt.Position);

                if (currentDist < radius)
                {
                    targets.Add(tgt);
                }
            }

            return targets.ToArray();
        }

        private void TrySetParasiteToUnit(IUnit unit, DataParasite dataParasite)
        {
            if (unit.Parasite == null)
            {
                unit.Parasite = new Parasite(dataParasite, unit);
                //unit.Parasite.OnBoomDeathParasite.AddListener(OnBoomParasite);
                //unit.Parasite.OnSpawnDethParasite.AddListener(OnSpawnParasite);
            }
            else unit.Parasite.AddNewParasite(dataParasite);
        }

        private void OnUnitDead(IUnit unit)
        {
            if (unit.Parasite == null) return;
            unit.Parasite.DeathEvent();
        }

        private void OnBoomParasite(Vector3 position, int damage)
        {
            //IUnit[] targets = FindUnitsInRadios(position, 1f, _unitManager.GetUnits());

            //foreach (var target in targets)
            //{
            //    target.DealDamage(Random.Range(1,damage));
            //}
        }

        private void OnSpawnParasite(DataParasite dataParasite, int amountParasites, Vector3 position, float radius)
        {
            TryGetParasites(dataParasite, (uint)amountParasites, position, radius);
        }

        #endregion
    }
}