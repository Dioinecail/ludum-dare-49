﻿namespace Project.Internal
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// MonoBehaviour который зависит от внутреннего таймера
    /// </summary>
    public abstract class TickBehaviour : MonoBehaviour
    {
        /// <summary>
        /// Replacement for Update. Override in child classes
        /// </summary>
        /// <param name="deltaTime"></param>
        protected abstract void OnTick(float deltaTime);

        protected virtual void OnEnable()
        {
            InternalTimerManager.onTick += OnTick;
        }

        protected virtual void OnDisable()
        {
            InternalTimerManager.onTick -= OnTick;
        }
    }
}