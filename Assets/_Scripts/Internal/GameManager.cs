﻿namespace Project.Internal
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Project.Units;

    public enum GameState
    {
        PLACING_UNITS,
        PLAYING,
        PAUSED,
        LEVEL_ENDED
    }

    public class GameManager : MonoBehaviour
    {
        public static event Action<GameState> onGameStateChanged;

        [SerializeField] private GameSettings settings;

        private GameState currentState = GameState.PAUSED;



        public void ForceChangeState(int stateIndex)
        {
            ChangeState((GameState)stateIndex);
        }

        private void Update()
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                if (currentState == GameState.PLAYING)
                    ChangeState(GameState.PAUSED);
                else if (currentState == GameState.PAUSED)
                    ChangeState(GameState.PLAYING);
            }
        }

        private void ChangeState(GameState state)
        {
            currentState = state;
            onGameStateChanged?.Invoke(currentState);
        }

        private void OnUnitsPlacementFinished(Vector2[] obj)
        {
            //ChangeState(GameState.PLAYING);
        }

        private void OnAllUnitsDied(Team obj)
        {
            ChangeState(GameState.LEVEL_ENDED);
        }

        private void OnEnable()
        {
            UnitPlacementManager.onPlacementFinished += OnUnitsPlacementFinished;
            UnitManager.onAllUnitsDied += OnAllUnitsDied;
        }

        private void OnDisable()
        {
            UnitPlacementManager.onPlacementFinished -= OnUnitsPlacementFinished;
            UnitManager.onAllUnitsDied -= OnAllUnitsDied;
        }
    }
}