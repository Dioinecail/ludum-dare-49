﻿namespace Project.Internal
{
    using System;
    using UnityEngine;

    public class InternalTimerManager : MonoBehaviour
    {
        public static Action<int> onTickRateChanged;
        public static Action<float> onTick;
        public static Action<bool> onRunningStateChanged;

        [SerializeField] private bool beginOnStart = true;
        [SerializeField] private int initialTickRate;
        [SerializeField] private int setTickRate;

        private bool isRunning;
        private bool isPaused;
        private int currentTickRate = 1;
        private int lastTickRate;



        public void ChangeTickRate(int newRate)
        {
            currentTickRate = newRate;
            onTickRateChanged?.Invoke(currentTickRate);
        }

        public void Start()
        {
            setTickRate = initialTickRate;
            ChangeTickRate(initialTickRate);
            isRunning = beginOnStart;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                isRunning = !isRunning;
                onRunningStateChanged?.Invoke(isRunning);
            }

            if (Input.GetKeyDown(KeyCode.Alpha1))
                ChangeTickRate(1);

            if (Input.GetKeyDown(KeyCode.Alpha2))
                ChangeTickRate(8);

            if (Input.GetKeyDown(KeyCode.Alpha3))
                ChangeTickRate(16);

            if (Input.GetKeyDown(KeyCode.Alpha4))
                ChangeTickRate(32);


            UpdateTick();
        }

        private void UpdateTick()
        {
            if (!isRunning || isPaused)
                return;

            for (int i = 0; i < currentTickRate; i++)
            {
                onTick?.Invoke(Time.deltaTime);
            }
        }

        private void OnValidate()
        {
            ChangeTickRate(setTickRate);
        }

        private void OnGameStateChanged(GameState state)
        {
            if(state == GameState.PLAYING)
            {
                isPaused = false;
            }
            else
            {
                isPaused = true;
            }
        }

        private void OnEnable()
        {
            GameManager.onGameStateChanged += OnGameStateChanged;
        }

        private void OnDisable()
        {
            GameManager.onGameStateChanged -= OnGameStateChanged;
        }
    }
}