﻿namespace Project.Grid
{
    using Project.Units;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public struct GridCell
    {
        public int x, y;
        public List<IUnit> containedUnits;

        public bool IsOccupied { get { return containedUnits.Count > 0; } }



        public GridCell(int x, int y)
        {
            this.x = x;
            this.y = y;
            this.containedUnits = new List<IUnit>();
        }
    }
}