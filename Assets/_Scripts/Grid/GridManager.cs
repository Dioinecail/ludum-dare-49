﻿namespace Project.Grid
{
    using Project.Units;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class GridManager
    {
        public GridCell[,] grid;
        public int Size { get; }
        public float CellSize { get; }



        public GridManager(int cellCount, float cellSize)
        {
            this.Size = cellCount;
            this.CellSize = cellSize;

            grid = new GridCell[cellCount, cellCount];

            for (int x = 0; x < cellCount; x++)
            {
                for (int y = 0; y < cellCount; y++)
                {
                    grid[x, y] = new GridCell(x, y);
                }
            }
        }

        public GridManager(float gridSize, float cellSize)
        {
            this.Size = Mathf.FloorToInt(gridSize / cellSize);
            this.CellSize = cellSize;

            grid = new GridCell[this.Size, this.Size];

            for (int x = 0; x < this.Size; x++)
            {
                for (int y = 0; y < this.Size; y++)
                {
                    grid[x, y] = new GridCell(x, y);
                }
            }
        }

        public void Clear()
        {
            for (int x = 0; x < Size; x++)
            {
                for (int y = 0; y < Size; y++)
                {
                    grid[x, y].containedUnits.Clear();
                }
            }
        }

        public void AddUnit(IUnit unit)
        {
            if (!TryGetCellIndex(unit.Position, out int x, out int y))
            {
                Debug.LogError($"{unit.Position} is out of bounds!");
                return;
            }

            grid[x, y].containedUnits.Add(unit);

            unit.onUnitMoved += OnUnitMoved;
            unit.onUnitDied += OnUnitDied;
        }

        public Vector2 GetCellPosition(int x, int y)
        {
            float offset = CellSize / 2f - (Size * CellSize) / 2f;
            float xPos = (x * CellSize) + offset;
            float yPos = (y * CellSize) + offset;

            return new Vector2(xPos, yPos);
        }

        public bool TryGetCellIndex(Vector2 position, out int x, out int y)
        {
            float offset = CellSize / 2f - (Size * CellSize) / 2f;

            x = Mathf.RoundToInt((position.x - offset) / CellSize);
            y = Mathf.RoundToInt((position.y - offset) / CellSize);

            return IsWithinGrid(x, y);
        }

        public List<IUnit> GetNeighbours(Vector2 position)
        {
            TryGetCellIndex(position, out int x, out int y);

            List<IUnit> neighbours = new List<IUnit>();

            for (int dX = -1; dX <= 1; dX++)
            {
                for (int dY = -1; dY <= 1; dY++)
                {
                    if (!IsWithinGrid(x + dX, y + dY))
                        continue;

                    neighbours.AddRange(grid[x + dX, y + dY].containedUnits);
                }
            }

            return neighbours;
        }

        public bool IsWithinGrid(int x, int y)
        {
            return x >= 0 && x < Size && y >= 0 && y < Size;
        }

        private void OnUnitDied(IUnit unit)
        {
            if (!TryGetCellIndex(unit.Position, out int x, out int y))
                return;

            grid[x, y].containedUnits.Remove(unit);
        }

        private void OnUnitMoved(IUnit unit, Vector2 oldPos, Vector2 newPos)
        {
            int oldX, oldY;
            int newX, newY;

            TryGetCellIndex(oldPos, out oldX, out oldY);
            TryGetCellIndex(newPos, out newX, out newY);

            if (oldX == newX && oldY == newY)
                return;

            if(oldX < grid.GetLength(0) && oldY < grid.GetLength(1))
                grid[oldX, oldY].containedUnits.Remove(unit);

            if (newX < grid.GetLength(0) && newY < grid.GetLength(1))
                grid[newX, newY].containedUnits.Add(unit);
        }
    }
}