﻿namespace Project.UI
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public abstract class MenuBase : MonoBehaviour
    {
        [SerializeField] private float hideMenuTimer = 1f;
        [SerializeField] private GameObject targetMenuGameobject;
        [SerializeField] protected UnityEvent onOpenMenu;
        [SerializeField] protected UnityEvent onCloseMenu;



        public virtual void OpenMenu()
        {
            targetMenuGameobject.SetActive(true);
            onOpenMenu?.Invoke();
        }

        public virtual void CloseMenu()
        {
            onCloseMenu?.Invoke();
            Invoke("HideMenuGameObject", hideMenuTimer);
        }

        public abstract MenuType GetMenuType();

        private void HideMenuGameObject()
        {
            targetMenuGameobject.SetActive(false);
        }
    }
}