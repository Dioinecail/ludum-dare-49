﻿namespace Project.UI
{
    using Project.Internal;
    using Project.Units;
    using Project.Utility;
    using Sirenix.OdinInspector;
    using System.Collections;
    using System.Collections.Generic;
    using TMPro;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.UI;

    public class GameplayMenu : MenuBase
    {
        #region PLAYING

        [Header("PLAYING PARAMETERS")]
        [SerializeField] private GameObject prefabUnitPlayer;
        [SerializeField] private GameObject[] prefabUnitEnemy;
        [SerializeField] private RectTransform unitsRoot;
        [SerializeField] private TMP_Text playerUnitsAliveText;
        [SerializeField] private TMP_Text enemyUnitsAliveText;
        [SerializeField] private TMP_Text currentGameSpeed;
        [SerializeField] private TMP_Text infoIsRunning;
        [SerializeField] private Color playerTeamColor;
        [SerializeField] private Color enemyTeamColor;

        private int playersAlive;
        private int enemiesAlive;

        private Dictionary<IUnit, GameObject> allUnits;

        [SerializeField] private UnityEvent onBeginPlayingEvent;
        [SerializeField] private UnityEvent onPauseEvent;

        #endregion

        #region PLACING

        [Header("PLACING PARAMETERS")]
        [SerializeField] private GameObject prefabUnitPosition;
        [SerializeField] private RectTransform unitPositionsRoot;
        [SerializeField] private TMP_Text playersPlacedText;

        [SerializeField] private UnityEvent onBeginPlacingEvent;
        [SerializeField] private UnityEvent onPlacingFinishedEvent;

        private GameObject[] placedUnitPositions;

        #endregion



        public override MenuType GetMenuType()
        {
            return MenuType.GAMEPLAY;
        }

        private void OnUnitSpawned(IUnit unit)
        {
            unit.onUnitMoved += OnUnitMoved;

            Vector2 uiPosition = Camera.main.WorldToScreenPoint(unit.Position);
            GameObject unitObject = null;

            if (unit.UnitTeam == Team.PLAYER)
            {
                unitObject = Instantiate(prefabUnitPlayer, uiPosition, Quaternion.identity, unitsRoot);
                unitObject.GetComponent<Animator>().SetFloat("Offset", Random.Range(0, 1f));

                playersAlive++;
                playerUnitsAliveText.text = $"<color=#6BCC66>Players</color>: {playersAlive}";
                unitObject.GetComponent<Image>().color = playerTeamColor;
            }
            else
            {
                unitObject = Instantiate(prefabUnitEnemy.GetRandom(), uiPosition, Quaternion.identity, unitsRoot);
                unitObject.GetComponent<Animator>().SetFloat("Offset", Random.Range(0, 1f));

                enemiesAlive++;
                enemyUnitsAliveText.text = $"<color=#CC3333>Enemies</color>: {enemiesAlive}";
                unitObject.GetComponent<Image>().color = enemyTeamColor;
            }

            unitObject.GetComponent<ToolTipUnitTarget>().SetData(unit);
            allUnits.Add(unit, unitObject);
        }

        private void OnUnitDied(IUnit unit)
        {
            unit.onUnitMoved -= OnUnitMoved;

            if (unit.UnitTeam == Team.PLAYER)
            {
                playersAlive--;
                playerUnitsAliveText.text = $"<color=#6BCC66>Players</color>: {playersAlive}";
            }
            else
            {
                enemiesAlive--;
                enemyUnitsAliveText.text = $"<color=#CC3333>Enemies</color>: {enemiesAlive}";
            }

            Destroy(allUnits[unit]);

            allUnits.Remove(unit);
        }

        private void OnUnitMoved(IUnit unit, Vector2 oldPos, Vector2 newPos)
        {
            allUnits[unit].transform.position = Camera.main.WorldToScreenPoint(newPos);
        }

        private void OnGameStateChanged(GameState state)
        {
            switch (state)
            {
                case GameState.PLACING_UNITS:
                    onBeginPlacingEvent?.Invoke();
                    break;
                case GameState.PLAYING:
                    onBeginPlayingEvent?.Invoke();
                    break;
                case GameState.PAUSED:
                    onPauseEvent?.Invoke();
                    break;
                case GameState.LEVEL_ENDED:
                    ResetUI();
                    break;
                default:
                    break;
            }
        }

        private void ResetUI()
        {
            //playersAlive = 0;
            enemiesAlive = 0;
            playersPlacedText.text = "";
            //playerUnitsAliveText.text = $"Players: {playersAlive}";
            enemyUnitsAliveText.text = $"<color=#CC3333>Enemies</color>: {enemiesAlive}";

            //foreach (var unit in allUnits.Values)
            //{
            //    Destroy(unit);
            //}

            //allUnits.Clear();
        }

        private void OnReset()
        {
            foreach (var unit in allUnits.Values)
            {
                Destroy(unit);
            }

            allUnits.Clear();
        }

        private void OnBeginPlacing(int total)
        {
            if(allUnits == null)
                allUnits = new Dictionary<IUnit, GameObject>();

            if(placedUnitPositions != null)
            {
                for (int i = 0; i < placedUnitPositions.Length; i++)
                {
                    if (placedUnitPositions[i] != null)
                        Destroy(placedUnitPositions[i]);
                }
            }

            placedUnitPositions = new GameObject[total];

            playersPlacedText.text = $"Placed {0} out of {total} units";
            onBeginPlacingEvent?.Invoke();
        }

        private void OnPlacingProgress(int current, int total, Vector2 unitPosition)
        {
            Vector2 uiPosition = Camera.main.WorldToScreenPoint(unitPosition);

            placedUnitPositions[current - 1] = Instantiate(prefabUnitPosition, uiPosition, Quaternion.identity, unitPositionsRoot); 

            playersPlacedText.text = $"Placed {current} out of {total} units";
        }

        private void OnPlacingFinished(Vector2[] positions)
        {
            onPlacingFinishedEvent?.Invoke();
        }

        private void OnTickrateChanged(int tickRate)
        {
            currentGameSpeed.text = $"Speed: x{tickRate}";
        }

        private void OnRunningStateChanged(bool isRunning)
        {
            if(isRunning)
                infoIsRunning.text = "Game is UNPAUSED\nPress SPACEBAR to pause";
            else
                infoIsRunning.text = "Game is PAUSED\nPress SPACEBAR to unpause";
        }

        private void OnEnable()
        {
            UnitManager.onUnitSpawned += OnUnitSpawned;
            UnitManager.onUnitDied += OnUnitDied;
            UnitManager.onReset += OnReset;

            UnitPlacementManager.onPlacingBegin += OnBeginPlacing;
            UnitPlacementManager.onPlacingProgress += OnPlacingProgress;
            UnitPlacementManager.onPlacementFinished += OnPlacingFinished;

            GameManager.onGameStateChanged += OnGameStateChanged;
            InternalTimerManager.onTickRateChanged += OnTickrateChanged;
            InternalTimerManager.onRunningStateChanged += OnRunningStateChanged;
        }

        private void OnDisable()
        {
            UnitManager.onUnitSpawned -= OnUnitSpawned;
            UnitManager.onUnitDied -= OnUnitDied;
            UnitManager.onReset -= OnReset;

            UnitPlacementManager.onPlacingBegin -= OnBeginPlacing;
            UnitPlacementManager.onPlacingProgress -= OnPlacingProgress;
            UnitPlacementManager.onPlacementFinished -= OnPlacingFinished;

            GameManager.onGameStateChanged -= OnGameStateChanged;
            InternalTimerManager.onTickRateChanged -= OnTickrateChanged;
            InternalTimerManager.onRunningStateChanged -= OnRunningStateChanged;
        }
    }
}