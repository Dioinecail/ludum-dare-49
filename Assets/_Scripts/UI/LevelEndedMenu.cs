﻿namespace Project.UI
{
    using Project.Units;
    using Project.Parasites;
    using System.Collections;
    using System.Collections.Generic;
    using TMPro;
    using UnityEngine;
    using Project.Internal;

    public class LevelEndedMenu : MenuBase
    {
        [SerializeField] private TMP_Text enemiesKillCountText;
        [SerializeField] private TMP_Text playersDiedCountText;
        [SerializeField] private TMP_Text parasitesUsedText;

        [SerializeField] bool isWinMenu;

        private int enemiesKilled;
        private int playersDied;
        private int parasitesUsed;

        private int enemiesKilledTotal;
        private int playersDiedTotal;
        private int parasitesUsedTotal;



        public override MenuType GetMenuType()
        {
            if (isWinMenu)
                return MenuType.WIN;
            else
                return MenuType.LOSE;
        }

        private void OnUnitDied(IUnit unit)
        {
            if (unit.UnitTeam == Team.PLAYER)
            {
                playersDied++;
                playersDiedTotal++;
                playersDiedCountText.text = $"Players died: {playersDied}/{playersDiedTotal} (level/total)";
            }
            else
            {
                enemiesKilled++;
                enemiesKilledTotal++;
                enemiesKillCountText.text = $"Enemies killed: {enemiesKilled}/{enemiesKilledTotal} (level/total)";
            }
        }

        private void OnParasiteUsed(Parasite parasite)
        {
            parasitesUsed++;
            parasitesUsedTotal++;
            parasitesUsedText.text = $"Parasites used: {parasitesUsed}/{parasitesUsedTotal} (level/total)";
        }

        private void OnGamestateChanged(GameState state)
        {
            switch (state)
            {
                case GameState.PLACING_UNITS:
                    parasitesUsed = 0;
                    playersDied = 0;
                    enemiesKilled = 0;
                    break;
            }
        }

        private void OnEnable()
        {
            UnitManager.onUnitDied += OnUnitDied;
            ParasitesManager.onParasiteUsed += OnParasiteUsed;
            GameManager.onGameStateChanged += OnGamestateChanged;
        }

        private void OnDisable()
        {
            UnitManager.onUnitDied -= OnUnitDied;
            ParasitesManager.onParasiteUsed -= OnParasiteUsed;
            GameManager.onGameStateChanged -= OnGamestateChanged;
        }
    }
}