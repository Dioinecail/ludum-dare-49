﻿namespace Project.UI
{
    using Project.Units;
    using Project.Utility;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    
    public enum MenuType
    {
        START,
        GAMEPLAY,
        WIN,
        LOSE
    }

    public class UserInterfaceManager : MonoBehaviour
    {
        private static ActionEvent<MenuType> onMenuSetEvent = new ActionEvent<MenuType>();

        [SerializeField] private MenuType initialMenuType = MenuType.START;

        private MenuBase[] menus;
        private MenuType currentlyActiveMenu = MenuType.LOSE;


        public void OpenURL(string url)
        {
            Application.OpenURL(url);
        }
        // LOL?!
        public static void ChangeMenu(MenuType targetMenu)
        {
            onMenuSetEvent.Invoke(targetMenu);
        }

        public void ChangeMenu(int menuType)
        {
            ChangeMenu((MenuType)menuType);
        }

        private void OnMenuChanged(MenuType targetMenu)
        {
            if (currentlyActiveMenu == targetMenu)
                return;

            for (int i = 0; i < menus.Length; i++)
            {
                if (menus[i].GetMenuType() == currentlyActiveMenu)
                    menus[i].CloseMenu();
                if (menus[i].GetMenuType() == targetMenu)
                    menus[i].OpenMenu();
            }

            currentlyActiveMenu = targetMenu;
        }

        private void OnAllUnitsDied(Team targetTeam)
        {
            if (targetTeam == Team.PLAYER)
                ChangeMenu(MenuType.LOSE);
            else
                ChangeMenu(MenuType.WIN);
        }

        private void OnEnable()
        {
            onMenuSetEvent.AddListener(OnMenuChanged);
            menus = GetComponentsInChildren<MenuBase>();

            ChangeMenu(initialMenuType);

            UnitManager.onAllUnitsDied += OnAllUnitsDied;
        }

        private void OnDisable()
        {
            onMenuSetEvent.RemoveAllListener();
            UnitManager.onAllUnitsDied -= OnAllUnitsDied;
        }
    }
}