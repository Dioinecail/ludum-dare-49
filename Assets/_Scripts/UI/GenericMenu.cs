﻿namespace Project.UI
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class GenericMenu : MenuBase
    {
        [SerializeField] private MenuType targetMenuType;



        public override MenuType GetMenuType()
        {
            return targetMenuType;
        }
    }
}