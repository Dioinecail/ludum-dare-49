﻿namespace Project.UI
{
    using Project.Units;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class ToolTipUnitTarget : ToolTipTarget
    {
        private IUnit target;



        public override void SetData(object data)
        {
            target = data as IUnit;
        }

        public override string[] GetData()
        {
            string[] data = new string[7];

            data[0] = $"Team:{ToolTipController.PrettyPrint(target.UnitTeam)}";
            data[1] = $"HP:{Mathf.RoundToInt(target.CurrentHP * target.GetStat(StatType.HP))}";
            data[2] = $"ATK:{target.GetStat(StatType.ATK)}";
            data[3] = $"DEF:{target.GetStat(StatType.DEF)}";
            data[4] = $"ATKSPEED:{target.GetStat(StatType.ATKSPEED)}";
            data[5] = $"MOVESPEED:{target.GetStat(StatType.MOVESPEED)}";
            data[6] = $"ATKRANGE:{target.GetStat(StatType.ATKRANGE)}";

            return data;
        }
    }
}