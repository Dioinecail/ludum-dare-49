﻿namespace Project.UI
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Project.Inventory;

    public class ToolTipItemTarget : ToolTipTarget
    {
        private DataItem target;



        public override void SetData(object data)
        {
            target = data as DataItem;
        }

        public override string[] GetData()
        {
            var parasite = target.Parasite;
            string[] data = new string[2 + parasite.ParasitesStats.Count];

            data[0] = $"Usable on: {ToolTipController.PrettyPrint(parasite.TeamType)}";
            data[1] = $"Radius: {target.Radius}m";

            for (int i = 0; i < parasite.ParasitesStats.Count; i++)
            {
                data[2 + i] = $"Effect: {parasite.ParasitesStats[i].ToString()}";
            }

            return data;
        }
    }
}