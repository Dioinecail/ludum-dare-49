﻿namespace Project.UI
{
    using Project.Utility;
    using System.Collections;
    using System.Collections.Generic;
    using System.Text;
    using TMPro;
    using UnityEngine;

    public class ToolTipController : MonoBehaviour
    {
        public static ActionEvent onHideToolTipObjects = new ActionEvent();
        public static ActionEvent onShowToolTipObjects = new ActionEvent();

        [SerializeField] private TMP_Text toolTipTarget;
        [SerializeField] private GameObject toolTipGO;
        [SerializeField] private RectTransform toolTipTransform;
        [SerializeField] private RectTransform toolTipBackgroundTransform;

        private ToolTipTarget currentTarget;



        public static string PrettyPrint(Units.Team team)
        {
            string tint = team == Units.Team.ENEMY ? "#CC3333" : "#6BCC66";
            return $"<color={tint}>{team}</color>";
        }

        public void DisplayToolTip(string[] data)
        {
            StringBuilder toolTipBuilder = new StringBuilder();

            foreach (var s in data)
            {
                toolTipBuilder.AppendLine(s);
            }

            toolTipTarget.text = toolTipBuilder.ToString();

            toolTipGO.SetActive(true);
        }

        public void HideToolTip()
        {
            toolTipGO.SetActive(false);
        }

        private void Update()
        {
            PositionToolTipTransform(Input.mousePosition);
            UpdateToolTip();

            if (Input.GetMouseButtonDown(0))
                onHideToolTipObjects.Invoke();
            else if (Input.GetMouseButtonUp(0))
                onShowToolTipObjects.Invoke();
        }

        private void UpdateToolTip()
        {
            if(currentTarget != null)
            {
                string[] data = GetData(currentTarget);

                DisplayToolTip(data);
            }
        }

        private void PositionToolTipTransform(Vector2 position)
        {
            toolTipTransform.position = position;

            if (Screen.width - position.x < 300)
                toolTipBackgroundTransform.localPosition = new Vector3(-149.0609f, toolTipBackgroundTransform.localPosition.y);
            else
                toolTipBackgroundTransform.localPosition = new Vector3(149.0609f, toolTipBackgroundTransform.localPosition.y);
        }

        private void OnToolTipHoverEnter(ToolTipTarget target)
        {
            if(currentTarget != target)
            {
                HideToolTip();
            }

            currentTarget = target;

            string[] data = GetData(currentTarget);

            DisplayToolTip(data);
        }

        private void OnToolTipHoverExit(ToolTipTarget target)
        {
            currentTarget = null;
            HideToolTip();
        }

        private string[] GetData(ToolTipTarget target)
        {
            string[] data = target.GetData();

            return data;
        }

        private void OnEnable()
        {
            ToolTipTarget.onHoverEnter.AddListener(OnToolTipHoverEnter);
            ToolTipTarget.onHoverExit.AddListener(OnToolTipHoverExit);
        }

        private void OnDisable()
        {
            ToolTipTarget.onHoverEnter.RemoveListener(OnToolTipHoverEnter);
            ToolTipTarget.onHoverExit.RemoveListener(OnToolTipHoverExit);
        }
    }
}