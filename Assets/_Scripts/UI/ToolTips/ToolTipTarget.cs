﻿namespace Project.UI
{
    using Project.Utility;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public abstract class ToolTipTarget : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public static ActionEvent<ToolTipTarget> onHoverEnter = new ActionEvent<ToolTipTarget>();
        public static ActionEvent<ToolTipTarget> onHoverExit = new ActionEvent<ToolTipTarget>();



        public void OnPointerEnter(PointerEventData eventData)
        {
            onHoverEnter.Invoke(this);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            onHoverExit.Invoke(this);
        }

        public abstract void SetData(object data);
        public abstract string[] GetData();


        private void SetRaycastable()
        {
            SetRaycastState(true);
        }

        private void SetNonRaycastable()
        {
            SetRaycastState(false);
        }

        private void SetRaycastState(bool state)
        {
            GetComponent<Graphic>().raycastTarget = state;
        }

        private void OnEnable()
        {
            ToolTipController.onShowToolTipObjects.AddListener(SetRaycastable);
            ToolTipController.onHideToolTipObjects.AddListener(SetNonRaycastable);
        }

        private void OnDisable()
        {
            ToolTipController.onShowToolTipObjects.RemoveListener(SetRaycastable);
            ToolTipController.onHideToolTipObjects.RemoveListener(SetNonRaycastable);
        }
    }
}