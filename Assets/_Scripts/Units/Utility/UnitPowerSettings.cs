﻿namespace Project.Units.Utility
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(menuName = "Settings/UnitPower")]
    public class UnitPowerSettings : ScriptableObject
    {
        [SerializeField] private int powerMultHP;
        [SerializeField] private int powerMultATK;
        [SerializeField] private int powerMultDEF;
        [SerializeField] private int powerMultATKRANGE;
        [SerializeField] private int powerMultMOVESPEED;
        [SerializeField] private int powerMultATKSPEED;



        public int GetMult(StatType type)
        {
            switch (type)
            {
                case StatType.HP:
                    return powerMultHP;
                case StatType.ATK:
                    return powerMultATK;
                case StatType.DEF:
                    return powerMultDEF;
                case StatType.ATKRANGE:
                    return powerMultATKRANGE;
                case StatType.MOVESPEED:
                    return powerMultMOVESPEED;
                case StatType.ATKSPEED:
                    return powerMultATKSPEED;
                default:
                    return 1;
            }
        }
    }
}