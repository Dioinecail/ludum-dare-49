﻿using System.Collections.Generic;

namespace Project.Units.Utility
{
    public static class UnitPowerCalculator
    {
        public static int Calculate(Dictionary<StatType, int> stats, UnitPowerSettings settings)
        {
            int sum = 0;

            foreach (var stat in stats)
            {
                sum += settings.GetMult(stat.Key) * stat.Value;
            }

            return sum;
        }
    }
}