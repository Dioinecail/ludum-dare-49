﻿namespace Project.Units
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    [Serializable]
    public class FloatEvent : UnityEvent<float> { }

    public class UnitPlacementManager : MonoBehaviour
    {
        public static event Action<int> onPlacingBegin;
        public static event Action<int, int, Vector2> onPlacingProgress;
        public static event Action<Vector2[]> onPlacementFinished;

        public float PlacementRadius { get => placementRadius; }

        [SerializeField] private float placementRadius;
        [SerializeField] private float placingDelay;

        private bool isPlacing;
        private List<Vector2> placementPositions;
        private int totalUnits;



        public void BeginPlacingUnits(int unitCount)
        {
            totalUnits = unitCount;
            placementPositions = new List<Vector2>(totalUnits);
            StartCoroutine(CoroutinePlacingUnits());
            onPlacingBegin?.Invoke(totalUnits);
        }

        private IEnumerator CoroutinePlacingUnits()
        {
            isPlacing = true;

            while(isPlacing)
            {
                TryPlaceUnits();

                yield return new WaitForSeconds(placingDelay);
            }
        }

        private void TryPlaceUnits()
        {
            if (!Input.GetMouseButton(0))
                return;

            Vector2 unitPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (unitPosition.magnitude < placementRadius)
            {
                placementPositions.Add(unitPosition);

                onPlacingProgress?.Invoke(placementPositions.Count, totalUnits, unitPosition);

                if(placementPositions.Count == totalUnits)
                {
                    isPlacing = false;
                    onPlacementFinished?.Invoke(placementPositions.ToArray());
                }
            }
        }

        private void OnDrawGizmos()
        {
            if (!isPlacing)
                return;

            Gizmos.color = Color.green;

            DrawCircle(Vector2.zero, placementRadius);

            if (placementPositions == null)
                return;

            Gizmos.color = Color.red;

            for (int i = 0; i < placementPositions.Count; i++)
            {
                Gizmos.DrawWireSphere(placementPositions[i], 0.1f);
            }
        }

        private void DrawCircle(Vector2 origin, float radius)
        {
            for (int i = 0; i < 50; i++)
            {
                float lerp1 = (float)i / 50;
                float lerp2 = (float)(i + 1) / 50;

                Vector2 pos1 = Quaternion.Euler(0, 0, lerp1 * 360) * (Vector2.up * radius);
                Vector2 pos2 = Quaternion.Euler(0, 0, lerp2 * 360) * (Vector2.up * radius);

                Gizmos.DrawLine(pos1, pos2);
            }
        }
    }
}