﻿namespace Project.Units
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Project.Parasites;

    public class UnitBase : IUnit
    {
        public event Action<IUnit, float> onHealthChanged;
        public event Action<IUnit, Vector2, Vector2> onUnitMoved;
        public event Action<IUnit> onUnitSpawned;
        public event Action<IUnit> onUnitDied;

        public int Id { get; private set; }
        public Vector2 Position { get; set; }
        public UnitState State { get; set; }
        public Team UnitTeam { get; set; }
        public IUnit Target { get; private set; }
        public Parasite Parasite 
        { 
            get => _parasite; 
            set { 
                _parasite = value; 
                _parasite.OnChangeParasite.AddListener(OnChangeParasite); 
                OnChangeParasite(); 
            } 
        }
        public float CurrentHP { get => (float)currentHealth / maxHealth; }

        private Parasite _parasite = null;
        private Dictionary<StatType, int> statDictionary;
        private int currentHealth;
        private int maxHealth;



        public UnitBase(int id, Vector2 position, Dictionary<StatType, int> stats, Team team)
        {
            Id = id;
            Position = position;
            CreateStats(stats);
            UnitTeam = team;
            maxHealth = GetStat(StatType.HP);
            currentHealth = maxHealth;
        }

        public void OnSpawn()
        {
            onUnitSpawned?.Invoke(this);
        }

        public int GetStat(StatType id)
        {
            if (statDictionary.ContainsKey(id))
                return statDictionary[id];
            else
                return 0;
        }

        public void DealDamage(int damage)
        {
            currentHealth -= damage;
            currentHealth = Mathf.Max(currentHealth, 0);

            onHealthChanged?.Invoke(this, (float)currentHealth / maxHealth);

            if (currentHealth == 0)
                OnUnitDied();
        }

        public void SetTarget(IUnit target)
        {
            Target = target;
            target.onUnitDied += OnTargetDied;
        }

        public void Move(Vector2 newPosition)
        {
            Vector2 oldPos = Position;
            Vector2 newPos = newPosition;

            Position = newPosition;

            onUnitMoved?.Invoke(this, oldPos, newPos);
        }

        private void CreateStats(Dictionary<StatType, int> stats)
        {
            statDictionary = new Dictionary<StatType, int>();

            foreach (var stat in stats)
            {
                statDictionary.Add(stat.Key, stat.Value);
            }
        }

        private void OnTargetDied(IUnit target)
        {
            target.onUnitDied -= OnTargetDied;
            Target = null;
        }

        private void OnUnitDied()
        {
            onUnitDied?.Invoke(this);
        }

        private void OnChangeParasite()
        {
            if (_parasite.NewParasitesStats.Count == 0) return;

            lock (_parasite.NewParasitesStats)
            {
                foreach (var stat in _parasite.NewParasitesStats)
                {
                    switch (stat.StatOperation)
                    {
                        case StatOperation.Add:
                            statDictionary[stat.StatType] = Mathf.Clamp(statDictionary[stat.StatType] + stat.Value, int.MinValue, int.MaxValue);
                            break;
                        case StatOperation.Divide:
                            if (stat.Value != 0)
                            {
                                statDictionary[stat.StatType] = Mathf.Clamp(Mathf.CeilToInt(statDictionary[stat.StatType] / stat.Value), int.MinValue, int.MaxValue);
                            }
                            break;
                        case StatOperation.Multiply:
                            statDictionary[stat.StatType] = Mathf.Clamp(statDictionary[stat.StatType] * stat.Value, int.MinValue, int.MaxValue);
                            break;
                        case StatOperation.Subtract:
                            statDictionary[stat.StatType] = Mathf.Clamp(statDictionary[stat.StatType] - stat.Value, int.MinValue, int.MaxValue);
                            break;
                    }

                    if (stat.StatType == StatType.HP)
                    {
                        var hpOffset = GetStat(StatType.HP) - maxHealth;
                        maxHealth -= hpOffset;
                        DealDamage(hpOffset);
                    }
                }
            }

            _parasite.NewParasitesStats.Clear();
        }
    }
}