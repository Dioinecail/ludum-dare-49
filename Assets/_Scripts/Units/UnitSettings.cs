﻿namespace Project.Units
{
    using Project.Units.Utility;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public enum StatType
    {
        HP,
        ATK,
        DEF,
        ATKRANGE,
        MOVESPEED,
        ATKSPEED
    }

    [CreateAssetMenu]
    public class UnitSettings : ScriptableObject
    {
        [SerializeField] private float moveSpeedMult;
        [SerializeField] private float attackSpeedMult;
        [SerializeField] private float attackRangeMult;

        public float MoveSpeedMult { get => moveSpeedMult; }
        public float AttackSpeedMult { get => attackSpeedMult; }
        public float AttackRangeMult { get => attackRangeMult; }
    }
}