﻿namespace Project.Units
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(menuName = "Settings/UnitStateSettings")]
    public class UnitStateSettings : ScriptableObject
    {
        public float unitAvoidancePower = 1;
        public float avoidanceRadius = 1;
    }
}