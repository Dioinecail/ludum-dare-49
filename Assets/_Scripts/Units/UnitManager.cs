﻿namespace Project.Units
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using Project.Internal;
    using Project.Grid;
    using UnityEngine.Events;
    using Project.Levels;

    /// <summary>
    /// UnitManager занимается только созданием юнитов и отслеживанием их жизни/смерти
    /// </summary>
    public class UnitManager : TickBehaviour
    {
        public static event Action<IUnit> onUnitSpawned;
        public static event Action<IUnit> onUnitDied;
        public static event Action<Team> onAllUnitsDied;
        public static event Action<Team, int> onUnitsCountChanged;
        public static event Action onReset;

        [SerializeField] private UnitSettings settings;
        [SerializeField] private UnitStateSettings stateSettings;
        [SerializeField] private UnitPlacementManager placementManager;
        [SerializeField] private float gridCellSize = 0.5f;
        [SerializeField] private float boundX, boundY;
        public GridManager GridManager { get; private set; }

        private Dictionary<Team, Dictionary<int, IUnit>> aliveUnits;
        private int internalIdCounter;
        private UnitStateManager stateManager;
        private Vector2[] unitPositions;
        private DataLevel currentLevel;



        public void Reset()
        {
            stateManager = new UnitStateManager(settings, stateSettings, this, boundX, boundY);
            aliveUnits = new Dictionary<Team, Dictionary<int, IUnit>>();
            aliveUnits.Add(Team.PLAYER, new Dictionary<int, IUnit>());
            aliveUnits.Add(Team.ENEMY, new Dictionary<int, IUnit>());
            internalIdCounter = 0;

            GC.Collect();

            CreateGrid(boundX, boundY);
            onReset?.Invoke();
        }

        public void SetSettings(DataLevel newLevel)
        {
            currentLevel = newLevel;
        }

        public void StartPlacingUnits()
        {
            StartPlacingUnits((int)currentLevel.AmountUnits[Team.PLAYER].amountUnits);
        }

        public void StartPlacingUnits(int count)
        {
            UnitPlacementManager.onPlacementFinished += OnPlacementReady;
            placementManager.BeginPlacingUnits(count);
        }

        public void SpawnPlayerUnits()
        {
            for (int i = 0; i < currentLevel.AmountUnits[Team.PLAYER].amountUnits; i++)
            {
                IUnit unit = CreateUnit(unitPositions[i], Team.PLAYER);

                onUnitSpawned?.Invoke(unit);
                unit.onUnitDied += OnUnitDied;

                GridManager.AddUnit(unit);

                aliveUnits[Team.PLAYER].Add(unit.Id, unit);
            }

            onUnitsCountChanged?.Invoke(Team.PLAYER, aliveUnits[Team.PLAYER].Count);
        }

        public void SpawnEnemyUnits()
        {
            for (int i = 0; i < currentLevel.AmountUnits[Team.ENEMY].amountUnits; i++)
            {
                IUnit unit = CreateUnit(GetRandomPosition(placementManager.PlacementRadius, boundX), Team.ENEMY);

                onUnitSpawned?.Invoke(unit);
                unit.onUnitDied += OnUnitDied;

                GridManager.AddUnit(unit);

                aliveUnits[Team.ENEMY].Add(unit.Id, unit);
            }

            onUnitsCountChanged?.Invoke(Team.ENEMY, aliveUnits[Team.ENEMY].Count);
        }

        public void SpawnUnits(Dictionary<Team,Levels.DataLevelUnits> units)
        {
            for (int i = 0; i < units[Team.ENEMY].amountUnits; i++)
            {
                IUnit unit = CreateUnit(GetRandomPosition(placementManager.PlacementRadius, boundX), Team.ENEMY, units[Team.ENEMY].stats);

                onUnitSpawned?.Invoke(unit);
                unit.onUnitDied += OnUnitDied;

                GridManager.AddUnit(unit);

                aliveUnits[Team.ENEMY].Add(unit.Id, unit);
            }

            onUnitsCountChanged?.Invoke(Team.ENEMY, aliveUnits[Team.ENEMY].Count);

            for (int i = 0; i < units[Team.PLAYER].amountUnits; i++)
            {
                IUnit unit = CreateUnit(unitPositions[i], Team.PLAYER, units[Team.PLAYER].stats);

                onUnitSpawned?.Invoke(unit);
                unit.onUnitDied += OnUnitDied;

                GridManager.AddUnit(unit);

                aliveUnits[Team.PLAYER].Add(unit.Id, unit);
            }

            onUnitsCountChanged?.Invoke(Team.PLAYER, aliveUnits[Team.PLAYER].Count);
        }

        public IUnit[] GetUnits(Team target)
        {
            if (aliveUnits.ContainsKey(target))
                return aliveUnits[target].Values.ToArray();
            else
                return null;
        }

        public IUnit[] GetUnits()
        {
            int totalUnits = 0;

            foreach (var units in aliveUnits)
            {
                totalUnits += units.Value.Count;
            }

            List<IUnit> allUnits = new List<IUnit>(totalUnits);

            foreach (var units in aliveUnits)
            {
                allUnits.AddRange(units.Value.Values);
            }

            return allUnits.ToArray();
        }

        public IUnit SpawnCustomUnit(Vector2 position, Team targetTeam, Dictionary<StatType, int> customStats)
        {
            IUnit unit = CreateUnit(position, targetTeam, customStats);

            onUnitSpawned?.Invoke(unit);
            unit.onUnitDied += OnUnitDied;
            GridManager.AddUnit(unit);

            aliveUnits[targetTeam].Add(unit.Id, unit);

            onUnitsCountChanged?.Invoke(targetTeam, aliveUnits[targetTeam].Count);

            return unit;
        }

        private void Start()
        {
            stateManager = new UnitStateManager(settings, stateSettings, this, boundX, boundY);
            aliveUnits = new Dictionary<Team, Dictionary<int, IUnit>>();
            aliveUnits.Add(Team.PLAYER, new Dictionary<int, IUnit>());
            aliveUnits.Add(Team.ENEMY, new Dictionary<int, IUnit>());
            internalIdCounter = 0;

            CreateGrid(boundX, boundY);
        }

        protected override void OnTick(float deltaTime)
        {
            foreach (var unitTeam in aliveUnits.Values)
            {
                foreach (IUnit unit in unitTeam.Values)
                {
                    stateManager.ProcessState(unit, deltaTime);
                }
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            GameManager.onGameStateChanged += OnGameStateChanged;
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            GameManager.onGameStateChanged -= OnGameStateChanged;
        }

        private void OnGameStateChanged(GameState state)
        {
            if(state == GameState.PLACING_UNITS)
            {
                //Reset();

                foreach (var unitTeam in aliveUnits.Values)
                {
                    foreach (var unit in unitTeam.Values)
                    {
                        if (Mathf.Approximately(unit.CurrentHP, 0))
                            aliveUnits[unit.UnitTeam].Remove(unit.Id);
                    }
                }

                GC.Collect();


            }
        }

        private void OnUnitDied(IUnit unit)
        {
            unit.onUnitDied -= OnUnitDied;
            onUnitDied?.Invoke(unit);

            Team unitTeam = unit.UnitTeam;

            aliveUnits[unitTeam].Remove(unit.Id);
            onUnitsCountChanged?.Invoke(unitTeam, aliveUnits[unitTeam].Values.Count);

            if (aliveUnits[unitTeam].Keys.Count == 0)
                onAllUnitsDied?.Invoke(unitTeam);
        }

        private IUnit CreateUnit(Vector2 position, Team team)
        {
            return CreateUnit(position, team, currentLevel.AmountUnits[team].stats);
        }

        private IUnit CreateUnit(Vector2 position, Team team, Dictionary<StatType, int> customStats)
        {
            return new UnitBase(GetId(), position, customStats, team);
        }

        private int GetId()
        {
            int newId = internalIdCounter;

            internalIdCounter++;

            return newId;
        }

        private Vector3 GetRandomPosition(float minRadius, float maxRadius)
        {
            float radius = UnityEngine.Random.Range(minRadius, maxRadius);

            return Quaternion.Euler(0, 0, UnityEngine.Random.Range(0, 360)) * Vector2.up * radius;
        }

        private void OnPlacementReady(Vector2[] positions)
        {
            UnitPlacementManager.onPlacementFinished -= OnPlacementReady;

            unitPositions = positions;
        }

        private void CreateGrid(float boundX, float boundY)
        {
            if (GridManager != null)
            {
                GridManager.Clear();
                return;
            }

            GridManager = new GridManager(boundX * 2, gridCellSize);
        }
    }
}