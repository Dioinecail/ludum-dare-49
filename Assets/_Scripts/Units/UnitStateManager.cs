﻿namespace Project.Units
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    /// <summary>
    /// UnitStateManager занимается процессом состояний юнитов, например перемещения и атаки
    /// </summary>
    public class UnitStateManager
    {
        public bool enableDebug;

        private UnitSettings settings;
        private UnitStateSettings stateSettings;
        private UnitManager manager;

        private Dictionary<int, float> attackTimers;
        private float boundX, boundY;



        public UnitStateManager(UnitSettings settings, UnitStateSettings stateSettings, UnitManager manager, float boundX, float boundY)
        {
            this.stateSettings = stateSettings;
            this.settings = settings;
            this.manager = manager;
            this.boundX = boundX;
            this.boundY = boundY;

            attackTimers = new Dictionary<int, float>();
        }

        public void ProcessState(IUnit unit, float deltaTime)
        {
            switch (unit.State)
            {
                case UnitState.SEEK:
                    OnSeek(unit, deltaTime);
                    break;
                case UnitState.ATTACK:
                    OnAttack(unit, deltaTime);
                    break;
                default:
                    break;
            }
        }

        // Change this to separate classes later
        private void OnSeek(IUnit unit, float deltaTime)
        {
            IUnit[] opposingUnits = null;

            switch (unit.UnitTeam)
            {
                case Team.PLAYER:
                    opposingUnits = manager.GetUnits(Team.ENEMY);
                    break;
                case Team.ENEMY:
                    opposingUnits = manager.GetUnits(Team.PLAYER);
                    break;
            }

            IUnit target = FindClosestUnit(unit, opposingUnits);

            if (target == null)
                return;

            Move(unit, target, deltaTime);

            if (CheckDistance(unit, target))
            {
                unit.SetTarget(target);
                unit.State = UnitState.ATTACK;
            }
        }

        // Change this to separate classes later
        private void OnAttack(IUnit unit, float deltaTime)
        {
            if (unit.Target == null)
            {
                // If the target died somehow
                unit.State = UnitState.SEEK;
                return;
            }

            if (!attackTimers.ContainsKey(unit.Id))
                attackTimers.Add(unit.Id, 0);

            attackTimers[unit.Id] += unit.GetStat(StatType.ATKSPEED) * settings.AttackSpeedMult * deltaTime;

            if(attackTimers[unit.Id] > 1)
            {
                // Attack ready HIT
                int damage = CalculateDamage(unit, unit.Target);
                unit.Target.DealDamage(damage);

                attackTimers[unit.Id] = 0;
                unit.State = UnitState.SEEK;
            }
        }

        private int CalculateDamage(IUnit unit, IUnit target)
        {
            int attack = unit.GetStat(StatType.ATK);
            int def = unit.Target.GetStat(StatType.DEF);

            // Change this to proper calculations later
            return Mathf.Max(attack - def, 0);
        }

        private void Move(IUnit src, IUnit target, float deltaTime)
        {
            if (float.IsNaN(src.Position.x) || float.IsNaN(target.Position.x))
                return;

            Vector2 direction = (target.Position - src.Position).normalized;

            Vector2 newPosition = src.Position + direction;

            List<IUnit> neighbours = manager.GridManager.GetNeighbours(src.Position);

            newPosition = ClampWithinBounds(CorrectPosition(src, newPosition, neighbours));

            Vector2 result = Vector2.MoveTowards(src.Position, newPosition, src.GetStat(StatType.MOVESPEED) * settings.MoveSpeedMult * deltaTime);

            src.Move(result);
        }

        private Vector2 CorrectPosition(IUnit src, Vector2 newPosition, List<IUnit> neighbours)
        {
            if (neighbours.Count == 1)
                return newPosition;

            int validNeighbours = 0;
            Vector2 result = Vector2.zero;

            foreach (IUnit unit in neighbours)
            {
                if (unit == src)
                    continue;

                if ((unit.Position - src.Position).magnitude > GetAvoidanceRadius())
                    continue;

                Vector2 direction = src.Position - unit.Position;

                Vector2 scaledDirection = ScaleDirection(direction.normalized, direction.magnitude);

                validNeighbours++;
                result += scaledDirection;

                if(enableDebug)
                {
                    Vector2 srcPos = Camera.main.WorldToScreenPoint(src.Position);
                    Vector2 neighbourPos = Camera.main.WorldToScreenPoint(unit.Position);

                    Debug.DrawLine(srcPos, neighbourPos, Color.yellow, 0.1f);
                }
            }

            if (validNeighbours == 0)
                return newPosition;

            result /= (validNeighbours);

            Vector2 perpendicularCorrection = Quaternion.Euler(0, 0, 90) * (newPosition - src.Position);

            if (enableDebug)
            {
                Vector2 srcPos = Camera.main.WorldToScreenPoint(src.Position);
                Vector2 resultPos = Camera.main.WorldToScreenPoint(newPosition + (result * GetAvoidancePower()));
                Debug.DrawRay(srcPos, result * GetAvoidancePower(), Color.white, 0.1f);
                Debug.DrawLine(srcPos, resultPos, Color.green, 0.1f);
            }

            return newPosition + (result * GetAvoidancePower() + perpendicularCorrection);
        }

        // CHANGE TO SECTOR BASED SEARCH IN PRODUCTION (check Game Programming AI book as ref)
        private IUnit FindClosestUnit(IUnit source, IUnit[] targets)
        {
            float distance = float.PositiveInfinity;
            IUnit target = null;

            Vector3 srcPos = source.Position;

            foreach (IUnit tgt in targets)
            {
                float currentDist = Vector3.Distance(srcPos, tgt.Position);

                if (currentDist < distance)
                {
                    distance = currentDist;
                    target = tgt;
                }
            }

            return target;
        }

        private bool CheckDistance(IUnit unit, IUnit target)
        {
            float checkDist = unit.GetStat(StatType.ATKRANGE) * settings.AttackRangeMult;

            return Vector3.Distance(unit.Position, target.Position) < checkDist;
        }

        private float GetAvoidancePower()
        {
            return stateSettings.unitAvoidancePower;
        }

        private float GetAvoidanceRadius()
        {
            return stateSettings.avoidanceRadius;
        }

        private Vector2 ScaleDirection(Vector2 direction, float magnitude)
        {
            if (magnitude < 0.01f)
                return direction;
            else
                return direction / magnitude;
        }

        private Vector2 ClampWithinBounds(Vector2 pos)
        {
            float x = Mathf.Clamp(pos.x, -boundX + 0.1f, boundX - 0.1f);
            float y = Mathf.Clamp(pos.y, -boundY + 0.1f, boundY - 0.1f);

            return new Vector2(x, y);
        }
    }
}