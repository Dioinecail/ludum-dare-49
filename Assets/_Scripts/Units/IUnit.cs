﻿namespace Project.Units
{
    using System;
    using UnityEngine;

    public enum UnitState
    {
        SEEK,
        ATTACK
    }

    public enum Team
    {
        PLAYER,
        ENEMY
    }

    public interface IUnit
    {
        event Action<IUnit, float> onHealthChanged;
        event Action<IUnit, Vector2, Vector2> onUnitMoved;
        event Action<IUnit> onUnitSpawned;
        event Action<IUnit> onUnitDied;
    
        int Id { get; }
        Vector2 Position { get; set; }
        UnitState State { get; set; }
        Team UnitTeam { get; set; }
        IUnit Target { get; }
        Parasites.Parasite Parasite { get; set; }
        float CurrentHP { get; }

        int GetStat(StatType id);
        void DealDamage(int damage);
        void SetTarget(IUnit target);
        void Move(Vector2 newPosition);
    }
}