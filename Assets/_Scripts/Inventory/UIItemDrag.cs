﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Project.Utility;
using Sirenix.OdinInspector;
using Project.UI;

namespace Project.Inventory
{
    public class UIItemDrag : MonoBehaviour
    {
        #region Inspector's Fields

        [SerializeField, Required] private Image _iconImage;
        [SerializeField, Required] private Image _iconImageOnDrag;

        [SerializeField] private Canvas _canvas;
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private RectTransform _rectTransform;

        private ItemContainer _currentItem;
        private DragObjectEvents _currentDrag;

        #endregion

        #region Properties

        public ItemContainer Item => _currentItem;
        public UIItemDropZone LastDropZone { get; private set; }

        public ActionEvent OnPointerDownEvent { get; private set; } = new ActionEvent();
        public ActionEvent OnBeginDragEvent { get; private set; } = new ActionEvent();
        public ActionEvent OnEndDragEvent { get; private set; } = new ActionEvent();
        public ActionEvent<UIItemDrag, PointerEventData> OnDragEvent { get; private set; } = new ActionEvent<UIItemDrag, PointerEventData>();

        #endregion

        #region Static

        public static ActionEvent<UIItemDrag> OnPointerUIItem { get; private set; } = new ActionEvent<UIItemDrag>();

        #endregion

        #region Public Methods

        public void SetItem(ItemContainer item)
        {
            _currentItem = item;
            if (_currentItem.ItemData != null)
            {
                Color tint = _currentItem.ItemData.Parasite.TeamType == Units.Team.PLAYER ? new Color(0, 0.8f, 0.4f, 1) : new Color(0.8f, 0.2f, 0.2f, 1);
                _iconImage.sprite = _currentItem.ItemData.Icon;
                _iconImage.color = tint;
                _iconImageOnDrag.sprite = _currentItem.ItemData.Icon;
                _iconImageOnDrag.color = tint;
                GetComponent<ToolTipItemTarget>().SetData(item.ItemData);
            }
        }

        public void SetPositionTo(Vector2 position)
        {
            _rectTransform.position = position;
        }

        public void ConnetDropZone(UIItemDropZone dropZone)
        {
            LastDropZone = dropZone;
        }

        public void TryConnectToLastDropZone()
        {
            LastDropZone.TrySetUIItem(this);
        }

        public void Destroy()
        {
            if (_currentDrag.IsDrag)
            {
                StartCoroutine(WaitEndDrag(() => GameObject.Destroy(this.gameObject)));
            }
            else
            {
                GameObject.Destroy(this.gameObject);
            }
        }

        #endregion

        #region Private Methods

        private void Awake()
        {
            if (_canvas == null) _canvas = GameObject.FindObjectOfType<Canvas>();
            if (_canvasGroup == null) _canvasGroup = this.gameObject.GetComponent<CanvasGroup>();
            if (_rectTransform == null) _rectTransform = this.transform as RectTransform;

            _currentDrag = gameObject.AddComponent<DragObjectEvents>();
            _currentDrag.OnPointerDownEvent.AddListener(OnPointerDown);
            _currentDrag.OnBeginDragEvent.AddListener(OnBeginDrag);
            _currentDrag.OnDragEvent.AddListener(OnDrag);
            _currentDrag.OnEndDragEvent.AddListener(OnEndDrag);

            DragObjectEvents.OnBeginDragAnyEvent.AddListener(SetOffRaycast);
            DragObjectEvents.OnEndDragAnyEvent.AddListener(SetOnRaycast);
        }

        private void OnDestroy()
        {
            OnPointerDownEvent.RemoveAllListener();
            OnBeginDragEvent.RemoveAllListener();
            OnEndDragEvent.RemoveAllListener();

            _currentDrag.OnPointerDownEvent.RemoveListener(OnPointerDown);
            _currentDrag.OnBeginDragEvent.RemoveListener(OnBeginDrag);
            _currentDrag.OnDragEvent.RemoveListener(OnDrag);
            _currentDrag.OnEndDragEvent.RemoveListener(OnEndDrag);

            DragObjectEvents.OnBeginDragAnyEvent.RemoveListener(SetOffRaycast);
            DragObjectEvents.OnEndDragAnyEvent.RemoveListener(SetOnRaycast);

            _currentDrag = null;
            _currentItem = null;
        }

        private void OnPointerDown(PointerEventData eventData)
        {
            OnPointerDownEvent.Invoke();
            OnPointerUIItem.Invoke(this);
        }

        private void OnBeginDrag(PointerEventData eventData)
        {
            SetOffRaycast();

            OnBeginDragEvent.Invoke();
            _iconImage.enabled = false;
            _iconImageOnDrag.enabled = true;
        }

        private void OnDrag(PointerEventData eventData)
        {
            _rectTransform.anchoredPosition += eventData.delta / _canvas.scaleFactor;
            OnDragEvent.Invoke(this, eventData);
        }

        private void OnEndDrag(PointerEventData eventData)
        {
            SetOnRaycast();
            OnEndDragEvent.Invoke();
            _iconImage.enabled = true;
            _iconImageOnDrag.enabled = false;
        }

        private void SetOffRaycast() => _canvasGroup.blocksRaycasts = false;
        private void SetOnRaycast() => _canvasGroup.blocksRaycasts = true;

        private IEnumerator WaitEndDrag(System.Action predicate)
        {
            yield return new WaitWhile(() => _currentDrag.IsDrag);

            predicate?.Invoke();
        }

        #endregion
    }
}