﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.EventSystems;
using Project.Utility;

namespace Project.Inventory
{
    public class UIInventoryController : MonoBehaviour
    {
        #region Static Events

        public static ActionEvent<UIItemDrag> onItemAdded = new ActionEvent<UIItemDrag>();

        #endregion

        #region Inspector's Fields

        [SerializeField, SceneObjectsOnly, Required] private RectTransform _goInventoryCellContainer;
        [SerializeField, AssetsOnly, Required] private UIItemDropZone _prefabInventoryCell;
        [SerializeField, AssetsOnly, Required] private UIItemDrag _prefabItem;
        [SerializeField, SceneObjectsOnly, Required] private RectTransform _goInventoryItemContainer;

        #endregion

        #region Private Fields

        private int _inventorySize;
        private List<UIItemDropZone> _inventoryCells = new List<UIItemDropZone>();

        #endregion

        #region Public Methods

        #endregion

        #region Private Methods
#if UNITY_EDITOR
        [Button]
        private void ActivateInventory()
        {
            InventoryManager.ActivateInventory(35);
        }

        [Button]
        private void AddSomeItems()
        {
            InventoryManager.TryAddItem(new ItemContainer("item-ATK"));
            InventoryManager.TryAddItem(new ItemContainer("item-ATK"));
            InventoryManager.TryAddItem(new ItemContainer("item-ATK"));
            InventoryManager.TryAddItem(new ItemContainer("item-DEF"));
            InventoryManager.TryAddItem(new ItemContainer("item-DEF"));
            InventoryManager.TryAddItem(new ItemContainer("item-DEF"));
        }
#endif

        private void Awake()
        {
            if (_goInventoryCellContainer == null || _prefabInventoryCell == null || _prefabItem == null)
            {
                Destroy(this);
                return;
            }
        }

        private void Start()
        {
            if (InventoryManager.IsActivateInventory)
            {
                CreateInventory(InventoryManager.InventorySize);
            }

            InventoryManager.OnActivateInventory.AddListener(OnActivateInventory);
            InventoryManager.OnDeactivateInventory.AddListener(OnDeactivateInventory);
            InventoryManager.OnChangeItemsInInventory.AddListener(OnChangeItemsInInventory);
            InventoryManager.OnChangeInventorySize.AddListener(OnChangeSizeInventory);
        }

        private void OnDestroy()
        {
            ClearUIInventory();

            InventoryManager.OnActivateInventory.RemoveListener(OnActivateInventory);
            InventoryManager.OnDeactivateInventory.RemoveListener(OnDeactivateInventory);
            InventoryManager.OnChangeItemsInInventory.RemoveListener(OnChangeItemsInInventory);
            InventoryManager.OnChangeInventorySize.RemoveListener(OnChangeSizeInventory);
        }

        private void OnActivateInventory()
        {
            CreateInventory(InventoryManager.InventorySize);
        }

        private void OnDeactivateInventory()
        {
            ClearUIInventory();
        }

        private void OnChangeItemsInInventory()
        {
            for (int i = 0; i < _inventorySize; i++)
            {
                CheckChangesInCell(i);
            }
        }

        private void OnChangeSizeInventory(int newSize)
        {
            CreateInventory(newSize);
        }

        private void ClearUIInventory()
        {
            if (_inventoryCells == null || _inventoryCells.Count == 0) return;

            foreach (var cell in _inventoryCells)
            {
                if (cell == null) continue;
                cell.OnFailTrySetUIItemToDropZone.RemoveAllListener();
                cell.OnChangeItemEvent.RemoveAllListener();
                cell.OnDropItemEvent.RemoveAllListener();
                cell.OnUnDropItemEvent.RemoveAllListener();
                cell.DestroyUIItem();
                if(cell.gameObject != null) GameObject.Destroy(cell.gameObject);
            }

            _inventoryCells.Clear();
        }

        private void CreateInventory(int size)
        {
            ClearUIInventory();

            if (size < 0) return;

            _inventorySize = size;

            for (int i = 0; i < _inventorySize; i++)
            {
                AddCell();
            }
        }

        private void CheckChangesInCell(int cellIndex)
        {
            if (cellIndex < 0 || cellIndex >= _inventorySize) return;

            var item = InventoryManager.TryGetItemIn(cellIndex);

            if (item != null)
            {
                if (_inventoryCells[cellIndex].UIItem == null)
                {
                    var uiItem = GameObject.Instantiate(_prefabItem, _goInventoryItemContainer).GetComponent<UIItemDrag>();
                    uiItem.SetItem(item);
                    onItemAdded.Invoke(uiItem);
                    _inventoryCells[cellIndex].SetUIItem(uiItem);
                }
                else if (_inventoryCells[cellIndex].UIItem.Item != item)
                {
                    _inventoryCells[cellIndex].UIItem.SetItem(item);
                }
            }
            else
            {
                if (_inventoryCells[cellIndex].UIItem != null)
                {
                    _inventoryCells[cellIndex].DestroyUIItem();
                }
            }
        }

        private void AddCell()
        {
            int index = _inventoryCells.Count;

            var dropzone = GameObject.Instantiate(_prefabInventoryCell, _goInventoryCellContainer).GetComponent<UIItemDropZone>();

            dropzone.OnFailTrySetUIItemToDropZone.AddListener(FailSetUIItemToDropZone);
            dropzone.OnChangeItemEvent.AddListener((x1,x2) => { OnChangeItemOnCell(index, x1, x2); });
            dropzone.OnDropItemEvent.AddListener((x) => { OnDropItemToCell(index, x); });
            dropzone.OnUnDropItemEvent.AddListener((x) => { OnUnDropItemToCell(index, x); });

            _inventoryCells.Add(dropzone);

            CheckChangesInCell(index);
        }

        private void OnChangeItemOnCell(int cellIndex, UIItemDrag itemOnCell, UIItemDrag itemDrop)
        {
            var indexLastDropZone = _inventoryCells.IndexOf(itemDrop.LastDropZone);

            OnUnDropItemToCell(cellIndex, itemOnCell);
            OnDropItemToCell(cellIndex, itemDrop);
            OnDropItemToCell(indexLastDropZone, itemOnCell);


            /*itemDrop.LastDropZone.SetUIItem(itemOnCell);
            InventoryManager.ChangeItemPosition(cellIndex, indexLastDropZone);

            _inventoryCells[cellIndex].SetUIItem(itemDrop);
            InventoryManager.TryAddItemTo(cellIndex, itemDrop.Item);*/
        }

        private void OnDropItemToCell(int cellIndex, UIItemDrag item)
        {
            _inventoryCells[cellIndex].SetUIItem(item);
            InventoryManager.TryAddItemTo(cellIndex, item.Item);
        }

        private void OnUnDropItemToCell(int cellIndex, UIItemDrag item)
        {
            _inventoryCells[cellIndex].RemoveUIItem();
            InventoryManager.TryRemoveItem(cellIndex);
        }

        private void FailSetUIItemToDropZone(UIItemDrag item)
        {
            if (InventoryManager.TryAddItem(item.Item))
            {
                GameObject.Destroy(item.gameObject);
            }
            else
            {
                //Пока непонятно что делать при заполненом инвенторе
                GameObject.Destroy(item.gameObject);
            }
        }

        private void OnReset()
        {
            ClearUIInventory();
        }

        private void OnEnable()
        {
            Units.UnitManager.onReset += OnReset;
        }

        private void OnDisable()
        {
            Units.UnitManager.onReset -= OnReset;
        }

        #endregion
    }
}