﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace Project.Inventory
{
    [System.Serializable]
    public class DataItem
    {
        #region Inspector's Fields

        [FoldoutGroup("$Id"), SerializeField] private string _id = "";
        [FoldoutGroup("$Id"), SerializeField] private bool _isDropToGame = true;
        [FoldoutGroup("$Id"), SerializeField, AssetsOnly] private Sprite _icon;
        [FoldoutGroup("$Id"), SerializeField] private float _radius = 1;
        [FoldoutGroup("$Id"), SerializeField] private uint _amountParasites = 1;
        [FoldoutGroup("$Id"), SerializeField] private Parasites.DataParasite _parasite = new Parasites.DataParasite();

        #endregion

        #region Properties

        public string Id => _id;
        public bool IsDropToGame => _isDropToGame;
        public Sprite Icon => _icon;

        public float Radius => _radius;
        public uint AmountParasites => _amountParasites;
        public Parasites.DataParasite Parasite => _parasite;

        #endregion
    }

    public class ItemContainer
    {
        #region Private Fields

        private string _dataId;

        #endregion

        #region Properties

        public string ItemID => _dataId;
        public DataItem ItemData => ItemsManager.GetItemData(_dataId);

        #endregion

        #region Private Fields

        public ItemContainer(string dataId) => _dataId = dataId;

        #endregion
    }
}