﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Project.Utility;

namespace Project.Inventory
{
    public class UIItemDropZone : MonoBehaviour
    {
        #region Private Fields

        private UIItemDrag _currentItem = null;

        private RectTransform _thisTransform;

        private DropObjectEvents _currentDrop;

        #endregion

        #region Properties

        private RectTransform ThisTransform 
        { 
            get
            {
                if (_thisTransform == null)
                    _thisTransform = this.transform as RectTransform;

                return _thisTransform;
            } 
        }
        public UIItemDrag UIItem => _currentItem;

        public ActionEvent<UIItemDrag> OnFailTrySetUIItemToDropZone { get; private set; } = new ActionEvent<UIItemDrag>();
        public ActionEvent<UIItemDrag, UIItemDrag> OnChangeItemEvent { get; private set; } = new ActionEvent<UIItemDrag, UIItemDrag>();
        public ActionEvent<UIItemDrag> OnUnDropItemEvent { get; private set; } = new ActionEvent<UIItemDrag>();
        public ActionEvent<UIItemDrag> OnDropItemEvent { get; private set; } = new ActionEvent<UIItemDrag>();

        #endregion

        #region Public Methods

        public void TrySetUIItem(UIItemDrag uIItem)
        {
            if (uIItem == null) return;

            if (_currentItem == null) OnDropItemEvent.Invoke(uIItem);
            else OnFailTrySetUIItemToDropZone.Invoke(uIItem);
        }

        public void SetUIItem(UIItemDrag uIItem)
        {
            if (uIItem == null) return;

            _currentItem = uIItem;
            _currentItem.OnBeginDragEvent.AddListener(OnUndrop);

            _currentItem.SetPositionTo(ThisTransform.position);

            _currentItem.ConnetDropZone(this);
        }

        public UIItemDrag RemoveUIItem()
        {
            _currentItem.OnBeginDragEvent.RemoveListener(OnUndrop);

            var item = _currentItem;
            _currentItem = null;

            return item;
        }

        public void DestroyUIItem()
        {
            if (_currentItem == null) return;

            _currentItem.OnBeginDragEvent.RemoveListener(OnUndrop);

            Destroy(_currentItem.gameObject);

            _currentItem = null;
        }

        #endregion

        #region Private Methods

        private void Awake()
        {
            _currentDrop = gameObject.AddComponent<DropObjectEvents>();
            _currentDrop.OnDropEvent.AddListener(OnDrop);
        }

        private void OnDestroy()
        {
            OnChangeItemEvent.RemoveAllListener();
            OnUnDropItemEvent.RemoveAllListener();
            OnDropItemEvent.RemoveAllListener();

            _currentDrop.OnDropEvent.RemoveListener(OnDrop);
            _currentDrop = null;

            DestroyUIItem();
        }

        private void OnDrop(PointerEventData eventData)
        {
            if (eventData.pointerDrag == null) return;

            var item = eventData.pointerDrag.GetComponent<UIItemDrag>();

            if (item == null) return;

            if (_currentItem != null) OnChangeItemEvent.Invoke(_currentItem, item);
            else OnDropItemEvent.Invoke(item);
        }

        private void OnUndrop()
        {
            OnUnDropItemEvent.Invoke(_currentItem); 
        }

        #endregion
    }
}