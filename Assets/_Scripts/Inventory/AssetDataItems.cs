﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Linq;

namespace Project.Inventory
{
    [CreateAssetMenu(fileName = "DataItems",menuName = "Project/DataItems",order = 1)]
    public class AssetDataItems : SerializedScriptableObject
    {
        [SerializeField] private List<DataItem> _items = new List<DataItem>();

        public DataItem GetItemData(string id) =>_items.FirstOrDefault(i => i.Id == id);
        
    }
}