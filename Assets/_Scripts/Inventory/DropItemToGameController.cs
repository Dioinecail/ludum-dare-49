﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Project.Utility;
using UnityEngine.UI;

namespace Project.Inventory
{
    public class DropItemToGameController : MonoBehaviour
    {
        #region Private Fields

        [SerializeField] private DropObjectEvents _dropObjectEvents;
        [SerializeField] private GameObject _dropRadius;

        #endregion

        #region Properties

        public static ActionEvent<DataItem, Vector2> OnDropItemToPosition { get; private set; } = new ActionEvent<DataItem, Vector2>();

        #endregion

        #region Private Methods

        private void Awake()
        {
            if (_dropObjectEvents == null) _dropObjectEvents = gameObject.GetComponent<DropObjectEvents>();
            if (_dropObjectEvents == null) _dropObjectEvents = gameObject.AddComponent<DropObjectEvents>();

            _dropObjectEvents.OnDropEvent.AddListener(OnDrop);
            UIInventoryController.onItemAdded.AddListener(OnItemAdded);
        }

        private void OnDestroy()
        {
            _dropObjectEvents.OnDropEvent.RemoveListener(OnDrop);
            UIInventoryController.onItemAdded.RemoveListener(OnItemAdded);
        }

        private void OnDrop(PointerEventData eventData)
        {
            if (eventData.pointerDrag == null) return;

            var item = eventData.pointerDrag.GetComponent<UIItemDrag>();

            if (item == null) return;

            TryDropItemToGame(item, eventData.position);
        }

        private void TryDropItemToGame(UIItemDrag uiItem, Vector2 position)
        {
            if (uiItem.Item == null)
            {
                Debug.LogError("[DropObjectToGameController/OnDrop/TryDropItemToGame] : UIItem not contained ItemContainer (is NULL)", uiItem.gameObject);
                uiItem.Destroy();
                return;
            }
            
            var item = uiItem.Item.ItemData;

            if (item == null)
            {
                Debug.LogError("[DropObjectToGameController/OnDrop/TryDropItemToGame] : DataItem is Null for itemId:" + uiItem.Item.ItemID, uiItem.gameObject);
                uiItem.Destroy();
                return;
            }
            
            if (!CheckItemTypeForDropToGame(item))
            {
                uiItem.TryConnectToLastDropZone();
                return;
            }

            OnDropItemToPosition.Invoke(item,position);
            uiItem.OnDragEvent.RemoveListener(OnItemDragged);
            uiItem.Destroy();
            _dropRadius.transform.localScale = Vector3.zero;
        }

        private bool CheckItemTypeForDropToGame(DataItem item) => item.IsDropToGame;

        private void OnItemAdded(UIItemDrag item)
        {
            item.OnDragEvent.AddListener(OnItemDragged);
        }

        private void OnItemDragged(UIItemDrag item, PointerEventData eventData)
        {
            Image dropRadiusIcon = _dropRadius.GetComponent<Image>();
            dropRadiusIcon.color = item.GetComponent<Image>().color;
            dropRadiusIcon.color = new Color(dropRadiusIcon.color.r, dropRadiusIcon.color.g, dropRadiusIcon.color.b, 0.35f);
            float radius = item.Item.ItemData.Radius;
            _dropRadius.transform.localScale = Vector3.one * radius;
            _dropRadius.transform.position = eventData.position;
        }

        #endregion
    }
}