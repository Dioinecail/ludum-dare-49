﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Project.Utility;

namespace Project.Inventory
{
    public static class InventoryManager
    {
        #region Private Fields

        private static int _inventorySize = 35;

        private static Dictionary<int, ItemContainer> _itemsOnInventory = new Dictionary<int, ItemContainer>();

        #endregion

        #region Properties

        public static int InventorySize => _inventorySize;
        public static bool IsActivateInventory { get; private set; } = false;

        public static ActionEvent OnActivateInventory { get; private set; } = new ActionEvent();
        public static ActionEvent OnDeactivateInventory { get; private set; } = new ActionEvent();
        public static ActionEvent<int> OnChangeInventorySize { get; private set; } = new ActionEvent<int>();
        public static ActionEvent OnChangeItemsInInventory { get; private set; } = new ActionEvent();

        #endregion

        #region Public Methods

        public static void DeactivateInventory()
        {
            if (!IsActivateInventory) return;

            _inventorySize = 0;
            _itemsOnInventory.Clear();

            IsActivateInventory = false;

            OnDeactivateInventory.Invoke();
#if UNITY_EDITOR
            //OnChangeItemsInInventory.RemoveListener(ToDebug);
#endif
        }

        public static void ActivateInventory(int size)
        {
            if (IsActivateInventory || size < 0) return;

            _inventorySize = size;

            IsActivateInventory = true;

            OnActivateInventory.Invoke();
#if UNITY_EDITOR
            //OnChangeItemsInInventory.AddListener(ToDebug);
#endif
        }

        public static void AddInventoryCells(int size)
        {
            if (!IsActivateInventory || size <= 0) return;

            _inventorySize += size;

            OnChangeInventorySize.Invoke(_inventorySize);
        }

        public static bool TryAddItem(ItemContainer item)
        {
            if (!IsActivateInventory) return false;

            if (_itemsOnInventory.Count == _inventorySize) return false;

            for(int i = 0; i < _inventorySize; i++)
                if (!_itemsOnInventory.ContainsKey(i))
                {
                    _itemsOnInventory.Add(i, item);
                    break;
                }

            OnChangeItemsInInventory.Invoke();

            return true;
        }

        public static bool TryAddItemTo(int cellIndex, ItemContainer item)
        {
            if (!IsActivateInventory) return false;

            if (cellIndex < 0 || cellIndex >= _inventorySize || _itemsOnInventory.ContainsKey(cellIndex)) return false;

            _itemsOnInventory.Add(cellIndex, item);

            OnChangeItemsInInventory.Invoke();

            return true;
        }

        public static ItemContainer TryGetItemIn(int cellIndex)
        {
            if (!IsActivateInventory) return null;

            if (cellIndex < 0 || !_itemsOnInventory.ContainsKey(cellIndex)) return null;

            return _itemsOnInventory[cellIndex];
        }

        public static ItemContainer TryRemoveItem(int index)
        {
            if (!IsActivateInventory) return null;

            if (index < 0 || !_itemsOnInventory.ContainsKey(index)) return null;

            var item = _itemsOnInventory[index];
            _itemsOnInventory.Remove(index);

            OnChangeItemsInInventory.Invoke();

            return item;
        }

        public static bool ChangeItemPosition(int index1, int index2)
        {
            if (!IsActivateInventory) return false;

            if (index1 < 0 || index2 < 0 || index1 >= _inventorySize || index2 >= _inventorySize) return false;

            ItemContainer item1 = null, item2 = null;
            if (_itemsOnInventory.ContainsKey(index1)) item1 = _itemsOnInventory[index1];
            if (_itemsOnInventory.ContainsKey(index2)) item2 = _itemsOnInventory[index2];

            if (_itemsOnInventory.ContainsKey(index1)) _itemsOnInventory[index1] = item2;
            if (_itemsOnInventory.ContainsKey(index2)) _itemsOnInventory[index2] = item1;

            OnChangeItemsInInventory.Invoke();

            return true;
        }

        #endregion

#if UNITY_EDITOR
        #region EDITOR

        public static void ToDebug()
        {
            string debug = "INVENTORY:\n";

            foreach(var item in _itemsOnInventory)
            {
                debug += item.Key + " : " + item.Value.ItemID + "\n";
            }

            UnityEngine.Debug.Log(debug);
        }

        #endregion
#endif
    }
}