﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Project.Inventory
{
    public class ItemsManager : MonoBehaviour
    {
        #region Private Fields

        public static ItemsManager Instance { get; private set; } = null;

        [SerializeField, AssetsOnly, Required] private AssetDataItems _dataItemsAsset;

        #endregion

        #region Public Methods

        public static DataItem GetItemData(string id) => Instance.GetItemDataThis(id);

        public DataItem GetItemDataThis(string id) => _dataItemsAsset.GetItemData(id);

        #endregion

        #region Private Fields

        private void Awake()
        {
            if(Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(this);
            }
        }

        private void OnDestroy()
        {
            if (Instance == this) Instance = null;
        }

        #endregion
    }
}