﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Project.Units;
using Project.Utility;

public class UnitParticles : MonoBehaviour
{
    [SerializeField] private GameObject prefabParticlesOnDeath;



    private void OnUnitDied(IUnit unit)
    {
        ParticleEmitter.EmitParticle(prefabParticlesOnDeath, unit.Position);
    }

    private void OnEnable()
    {
        UnitManager.onUnitDied += OnUnitDied;
    }

    private void OnDisable()
    {
        UnitManager.onUnitDied -= OnUnitDied;
    }
}
