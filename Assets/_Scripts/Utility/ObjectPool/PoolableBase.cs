﻿namespace Project.Utility
{
    using UnityEngine;

    public abstract class PoolableBase : MonoBehaviour, IPoolableObject
    {
        public event OnReturnToPool onReturnToPool;

        public float returnToPoolTimer = 5;



        [ContextMenu("Return to pool")]
        public virtual void ReturnToPool()
        {
            onReturnToPool?.Invoke(this);
            GameobjectPoolSystem.ReleaseObject(gameObject);
            CancelInvoke();
        }

        protected virtual void OnEnable()
        {
            if (returnToPoolTimer < 0)
                return;
            Invoke("ReturnToPool", returnToPoolTimer);
        }
    }
}