﻿namespace Project.Utility
{
    using System.Collections.Generic;
    using UnityEngine;
    using System.Linq;

    public static class GameobjectPoolSystem
    {
        private static Transform _staticRoot;

        private static Dictionary<string, HashSet<GameObject>> pools;
        private static Transform staticRoot
        {
            get
            {
                if (_staticRoot == null)
                {
                    GameObject root = new GameObject("Static_root");
                    _staticRoot = root.transform;
                    _staticRoot.position = Vector3.zero;
                }

                return _staticRoot;
            }
        }

        public static GameObject Instantiate(GameObject obj, Vector3 position, Quaternion rotation, bool setActive = true)
        {
            if (pools == null)
                pools = new Dictionary<string, HashSet<GameObject>>();

            if (!pools.ContainsKey(obj.name))
                pools[obj.name] = new HashSet<GameObject>();

            GameObject poolObject;

            if (pools[obj.name].Count > 0)
            {
                poolObject = pools[obj.name].ElementAt(0);
                pools[obj.name].Remove(poolObject);
            }
            else
            {
                poolObject = GameObject.Instantiate(obj, staticRoot);
                poolObject.name = obj.name;
            }

            poolObject.transform.position = position;
            poolObject.transform.rotation = rotation;

            poolObject.SetActive(setActive);

            return poolObject;
        }

        public static void ReleaseObject(GameObject obj)
        {
            if (pools[obj.name].Contains(obj))
            {
                Debug.Log("Object is already contained within the pool");
                return;
            }

            pools[obj.name].Add(obj);

            obj.SetActive(false);
        }

        public static void Clear()
        {
            pools.Clear();
            GameObject.Destroy(staticRoot.gameObject);
            _staticRoot = null;
        }
    }
}