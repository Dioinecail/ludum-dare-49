﻿namespace Project.Utility
{
    using System.Collections.Generic;
    using UnityEngine;

    public class ParticleEmitter
    {
        private static Dictionary<GameObject, ParticleSystem> particleDictionary = new Dictionary<GameObject, ParticleSystem>();



        public static void EmitParticle(GameObject prefab, Transform parent)
        {
            ParticleSystem particle = EmitParticle(prefab, parent.position);
            particle.transform.SetParent(parent);
        }

        public static ParticleSystem EmitParticle(GameObject prefab, Vector3 position)
        {
            return EmitParticle(prefab, position, Quaternion.identity);
        }

        public static ParticleSystem EmitParticle(GameObject prefab, Vector3 position, Quaternion rotation)
        {
            ParticleSystem particle;

            if (particleDictionary.ContainsKey(prefab))
            {
                particle = particleDictionary[prefab];
            }
            else
            {
                GameObject clone = Object.Instantiate(prefab, position, Quaternion.identity);
                particle = clone.GetComponent<ParticleSystem>();

                particleDictionary.Add(prefab, particle);
            }

            particle.gameObject.SetActive(true);
            particle.transform.position = position;
            particle.transform.rotation = rotation;
            particle.Play();

            return particle;
        }

        public static void StopParticle(GameObject prefab)
        {
            if (particleDictionary.ContainsKey(prefab))
            {
                if (particleDictionary[prefab] == null)
                {
                    particleDictionary.Remove(prefab);
                    return;
                }

                particleDictionary[prefab].transform.SetParent(null);
                particleDictionary[prefab].Stop();
            }
        }

        public static void Clear()
        {
            particleDictionary.Clear();
        }
    }
}