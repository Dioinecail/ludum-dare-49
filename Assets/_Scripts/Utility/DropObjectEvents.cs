﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Project.Utility
{
    public class DropObjectEvents : MonoBehaviour, IDropHandler
    {
        public ActionEvent<PointerEventData> OnDropEvent { get; private set; } = new ActionEvent<PointerEventData>();

        public void OnDrop(PointerEventData eventData)
        {
            if (eventData == null) return;
            //Debug.Log("[" + this.gameObject.name + "/DropObject/OnDrop]", this);

            OnDropEvent.Invoke(eventData);
        }

        private void OnDestroy()
        {
            OnDropEvent.RemoveAllListener();
        }
    }
}