﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Project.Utility
{
    public class DragObjectEvents : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        public static ActionEvent OnBeginDragAnyEvent { get; private set; } = new ActionEvent();
        public static ActionEvent OnEndDragAnyEvent { get; private set; } = new ActionEvent();

        public bool IsDrag { get; private set; } = false;
        public ActionEvent<PointerEventData> OnPointerDownEvent { get; private set; } = new ActionEvent<PointerEventData>();
        public ActionEvent<PointerEventData> OnBeginDragEvent { get; private set; } = new ActionEvent<PointerEventData>();
        public ActionEvent<PointerEventData> OnDragEvent { get; private set; } = new ActionEvent<PointerEventData>();
        public ActionEvent<PointerEventData> OnEndDragEvent { get; private set; } = new ActionEvent<PointerEventData>();

        public void OnPointerDown(PointerEventData eventData)
        {
            if (eventData == null) return;
            //Debug.Log("[" + this.gameObject.name + "/DragObject/OnPointerDown]", this);

            OnPointerDownEvent.Invoke(eventData);
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (eventData == null) return;
            //Debug.Log("[" + this.gameObject.name + "/DragObject/OnBeginDrag]", this);

            IsDrag = true;
            OnBeginDragEvent.Invoke(eventData);
            OnBeginDragAnyEvent.Invoke();
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (eventData == null) return;
            //Debug.Log("[" + this.gameObject.name + "/DragObject/OnDrag]", this);

            OnDragEvent.Invoke(eventData);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (eventData == null) return;
            //Debug.Log("[" + this.gameObject.name + "/DragObject/OnEndDrag]", this);

            IsDrag = false;
            OnEndDragEvent.Invoke(eventData);
            OnEndDragAnyEvent.Invoke();
        }

        private void OnDestroy()
        {
            OnPointerDownEvent.RemoveAllListener();
            OnBeginDragEvent.RemoveAllListener();
            OnDragEvent.RemoveAllListener();
            OnEndDragEvent.RemoveAllListener();
        }

    }
}