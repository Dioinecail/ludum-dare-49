﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Project.Utility;

namespace Project.Levels
{
    public enum LevelProgressionType
    {
        SEQUENCIAL,
        RANDOM
    }

    public class LevelManager : MonoBehaviour
    {
        #region Inspector's Fields

        [SerializeField] private Units.UnitManager _manager;
        [SerializeField] private List<AssetDataLevels> _allTypeLevels = new List<AssetDataLevels>();
        [SerializeField] private LevelProgressionType progressionType;

        #endregion

        #region Private Fields

        private bool _isActivateLevel = false;
        private int _numberLevels = -1;
        private int _numberLevel = 0;

        #endregion

        #region Properties

        public bool IsActivateLevels => _isActivateLevel;
        public ActionEvent OnDefeatLevels { get; private set; } = new ActionEvent();
        public ActionEvent OnWinAllLevels { get; private set; } = new ActionEvent();
        public ActionEvent OnWinCurrentLevel { get; private set; } = new ActionEvent();

        #endregion

        #region Public Methods

        public void ActivateLevels()
        {
            if (_isActivateLevel) return;
            _isActivateLevel = true;

            if (progressionType == LevelProgressionType.RANDOM)
                _numberLevels = Random.Range(0, _allTypeLevels.Count - 1);
            else if (progressionType == LevelProgressionType.SEQUENCIAL)
                _numberLevels++;
            _numberLevel = -1;

            Units.UnitManager.onAllUnitsDied += OnAllUnitsDied;
            Inventory.InventoryManager.ActivateInventory(18);

            NextLevel();
        }

        public void DeactivateLevels()
        {
            if (!_isActivateLevel) return;
            _isActivateLevel = false;

            Units.UnitManager.onAllUnitsDied -= OnAllUnitsDied;
            Inventory.InventoryManager.DeactivateInventory();
        }

        public void NextLevel()
        {
            Inventory.InventoryManager.ActivateInventory(18);
            _numberLevel++;
            var level = _allTypeLevels[_numberLevels].GetLevel(_numberLevel);
            StartCoroutine(AddToInventoryItems(level.StartingItems.ToArray()));

            _manager.SetSettings(level);
        }

        #endregion

        #region Private Methods

        private void OnDestroy()
        {
            DeactivateLevels();
        }

        private void FinishLevel()
        {
            if (_numberLevel + 1 >= _allTypeLevels[_numberLevels].AmountLevels)
            {
                OnWinAllLevels.Invoke();
                DeactivateLevels();
                return;
            }

            OnWinCurrentLevel.Invoke();

            var level = _allTypeLevels[_numberLevels].GetLevel(_numberLevel);
            StartCoroutine(AddToInventoryItems(level.VictoryItems.ToArray()));
        }

        // Coroutine hack to make it put items into proper place
        private IEnumerator AddToInventoryItems(string[] ids)
        {
            yield return new WaitForSeconds(1);

            foreach(var id in ids)
            {
                if(Inventory.ItemsManager.GetItemData(id) != null)
                    Inventory.InventoryManager.TryAddItem(new Inventory.ItemContainer(id));
            }
        }

        private void OnAllUnitsDied(Units.Team deadTeam)
        {
            if (deadTeam == Units.Team.ENEMY)
            {
                OnDefeatLevels.Invoke();
                DeactivateLevels();
            }
            else FinishLevel();
        }

        private void OnReset()
        {
            _numberLevels = -1;
            _numberLevel = -1;
            Inventory.InventoryManager.DeactivateInventory();
        }

        private void OnEnable()
        {
            Units.UnitManager.onReset += OnReset;   
        }

        private void OnDisable()
        {
            Units.UnitManager.onReset -= OnReset;
        }

        #endregion
    }
}