﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using Project.Units.Utility;

namespace Project.Levels
{
    [System.Serializable]
    public class DataLevel
    {
        [ShowInInspector, OdinSerialize] private Dictionary<Units.Team, DataLevelUnits> _amountUnits = new Dictionary<Units.Team, DataLevelUnits>();



        [SerializeField] private List<string> _startingItems;
        [SerializeField] private List<string> _victoryItems;

        public Dictionary<Units.Team, DataLevelUnits> AmountUnits => _amountUnits;

        public List<string> StartingItems => _startingItems;
        public List<string> VictoryItems => _victoryItems;



        public DataLevel(DataLevel template)
        {
            _amountUnits = new Dictionary<Units.Team, DataLevelUnits>();

            foreach (var item in template.AmountUnits)
            {
                _amountUnits.Add(item.Key, new DataLevelUnits(item.Value));
            }
            _startingItems = new List<string>();

            for (int i = 0; i < template._startingItems.Count; i++)
            {
                _startingItems.Add(new string(template._startingItems[i].ToCharArray()));
            }

            _victoryItems = new List<string>();

            for (int i = 0; i < template._victoryItems.Count; i++)
            {
                _victoryItems.Add(new string(template._victoryItems[i].ToCharArray()));
            }
        }
    }

    public class DataLevelUnits
    {
        public int totalPower;
        public uint amountUnits = 10;
        public Dictionary<Units.StatType, int> stats = new Dictionary<Units.StatType, int>();
        public uint statsOffset = 10;

        DataLevelUnits()
        {
            stats = new Dictionary<Units.StatType, int>();
            stats.Add(Units.StatType.HP, 100);
            stats.Add(Units.StatType.MOVESPEED, 100);
            stats.Add(Units.StatType.DEF, 100);
            stats.Add(Units.StatType.ATKSPEED, 100);
            stats.Add(Units.StatType.ATKRANGE, 100);
            stats.Add(Units.StatType.ATK, 100);
        }

        public DataLevelUnits(DataLevelUnits template)
        {
            amountUnits = template.amountUnits;
            statsOffset = template.statsOffset;

            stats = new Dictionary<Units.StatType, int>();

            foreach (var stat in template.stats)
            {
                stats.Add(stat.Key, stat.Value);
            }
        }

        public int GetStat(Units.StatType type)
        {
            return stats[type];
        }
    }
}