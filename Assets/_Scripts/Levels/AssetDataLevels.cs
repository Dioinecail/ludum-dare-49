﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using Project.Units.Utility;

namespace Project.Levels
{
    [CreateAssetMenu(fileName = "DataLevels", menuName = "Project/DataLevels", order = 1)]
    public class AssetDataLevels : SerializedScriptableObject
    {
        [OdinSerialize] private List<DataLevel> _levels = new List<DataLevel>();

        public int AmountLevels => _levels.Count;

        public List<DataLevel> Levels => _levels;

        public DataLevel GetLevel(int index)
        {
            if (_levels.Count <= index) return null;

            return _levels[index];
        }

        #region EDITOR

        [Button]
        private void DuplicateLastLevel()
        {
            DataLevel level = new DataLevel(_levels[_levels.Count - 1]);
            _levels.Add(level);
        }

        [Button]
        private void CreateDebugLevels()
        {
            for (int i = 0; i < 3; i++)
            {
                if(_levels.Count < i + 1)
                {
                    DataLevel level = new DataLevel(_levels[0]);
                    _levels.Add(level);
                }
                else
                {
                    _levels[i + 1] = new DataLevel(_levels[0]);
                }
            }

            _levels[1].AmountUnits[Units.Team.PLAYER].amountUnits = 5;
            _levels[1].AmountUnits[Units.Team.ENEMY].amountUnits = 5;

            _levels[2].AmountUnits[Units.Team.PLAYER].amountUnits = 25;
            _levels[2].AmountUnits[Units.Team.ENEMY].amountUnits = 25;

            _levels[3].AmountUnits[Units.Team.PLAYER].amountUnits = 105;
            _levels[3].AmountUnits[Units.Team.ENEMY].amountUnits = 105;
        }

#if UNITY_EDITOR

        UnitPowerSettings settings;

        private void OnValidate()
        {
            if (settings == null)
                settings = UnityEditor.AssetDatabase.LoadAssetAtPath<UnitPowerSettings>("Assets/Settings/Settings_UnitPower.asset");

            foreach (var item in _levels)
            {
                foreach (var data in item.AmountUnits)
                {
                    data.Value.totalPower = UnitPowerCalculator.Calculate(data.Value.stats, settings) * (int)data.Value.amountUnits;
                }
            }
        }

#endif

        #endregion
    }
}